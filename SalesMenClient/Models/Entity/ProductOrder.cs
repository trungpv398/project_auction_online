﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SalesMenClient.Models.Entity
{
    public class ProductOrder
    {
        public ProductAuction ProductAuction { get; set; }
        public Order Order { get; set; }


        public List<ProductOrder> GetListPOBySalesMan(int SaleManID)
        {
            ProductAuction proAu = new ProductAuction();
            Auction auction = new Auction();

            Order order = new Order();
            List<ProductOrder> listPO = new List<ProductOrder>();



            var dataPro = proAu.GetBySaleMan(SaleManID);
            var dataOrder = order.GetAllOrder();

            foreach(var pro in dataPro)
            {
                foreach(var ord in dataOrder)
                {
                    if(pro.ID == ord.ProAucId)
                    {
                        ProductOrder p = new ProductOrder();
                        p.Order = order.GetByOrderID(ord.ID);
                        p.ProductAuction = proAu.GetById(pro.ID);
                        listPO.Add(p);
                    }
                }
                
            }
            return listPO;
        }
    }
}