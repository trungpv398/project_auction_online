﻿using SalesMenClient.Models.Global;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;

namespace SalesMenClient.Models.Entity
{
    public class Type_Product
    {
        public int ID { get; set; }
        public string TypeProName { get; set; }
        public int Status { get; set; }
        public DateTime Created { get; set; }
        public IEnumerable<Brand> Brands { get; set; }
        public IEnumerable<Parameter> Parameters { get; set; }


        //Get Put Post Delete Api
        public IEnumerable<Type_Product> GetAll()
        {
            IEnumerable<Type_Product> typeList;
            HttpResponseMessage response = GlobalVariables.WebApiClient.GetAsync("TypeProduct").Result;
            typeList = response.Content.ReadAsAsync<IEnumerable<Type_Product>>().Result;
            return typeList;

        }

        public Type_Product GetById(int id)
        {
            Type_Product typePro;
            HttpResponseMessage response = GlobalVariables.WebApiClient.GetAsync("TypeProduct/" + id).Result;
            typePro = response.Content.ReadAsAsync<Type_Product>().Result;
            return typePro;
        }

        public Type_Product EditType(Type_Product c)
        {
            Type_Product typePro;
            HttpResponseMessage response = GlobalVariables.WebApiClient.PutAsJsonAsync("TypeProduct", c).Result;
            typePro = response.Content.ReadAsAsync<Type_Product>().Result;
            return typePro;
        }

        public Message AddType(Type_Product c)
        {
            Message typePro;
            HttpResponseMessage response = GlobalVariables.WebApiClient.PostAsJsonAsync("TypeProduct", c).Result;
            typePro = response.Content.ReadAsAsync<Message>().Result;
            var b = typePro;
            return typePro;
        }
        public Type_Product DeleteType(int id)
        {
            Type_Product typePro;
            HttpResponseMessage response = GlobalVariables.WebApiClient.DeleteAsync("TypeProduct/" + id).Result;
            typePro = response.Content.ReadAsAsync<Type_Product>().Result;
            return typePro;
        }

    }
}