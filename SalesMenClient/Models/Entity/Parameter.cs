﻿using SalesMenClient.Models.Global;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Net.Http;
using System.Web;

namespace SalesMenClient.Models.Entity
{
    public class Parameter
    {
        public int ID { get; set; }
        public string ParameterName { get; set; }
        public int Status { get; set; }
        public int TypeProID { get; set; }
        public DateTime Created { get; set; }
        [ForeignKey("TypeProID")]
        public Type_Product Type_Product { get; set; }
        public IEnumerable<ProAuParameter> ProAuParameters { get; set; }

        public IEnumerable<Parameter> GetAll()
        {
            IEnumerable<Parameter> data;
            HttpResponseMessage response = GlobalVariables.WebApiClient.GetAsync("Parameter").Result;
            data = response.Content.ReadAsAsync<IEnumerable<Parameter>>().Result;
            return data;

        }


        public IEnumerable<Parameter> GetByType(int typeProId)
        {
            IEnumerable<Parameter> data;
            HttpResponseMessage response = GlobalVariables.WebApiClient.GetAsync("Parameter?typeProId="+typeProId).Result;
            data = response.Content.ReadAsAsync<IEnumerable<Parameter>>().Result;
            return data;
        }

        public Parameter GetById(int id)
        {
            Parameter data;
            HttpResponseMessage response = GlobalVariables.WebApiClient.GetAsync("Parameter/" + id).Result;
            data = response.Content.ReadAsAsync<Parameter>().Result;
            return data;
        }

        public Parameter Edit(Parameter c)
        {
            Parameter data;
            HttpResponseMessage response = GlobalVariables.WebApiClient.PutAsJsonAsync("Parameter", c).Result;
            data = response.Content.ReadAsAsync<Parameter>().Result;
            return data;
        }

        public Message AddNew(Parameter c)
        {
            Message data;
            HttpResponseMessage response = GlobalVariables.WebApiClient.PostAsJsonAsync("Parameter", c).Result;
            data = response.Content.ReadAsAsync<Message>().Result;
            return data;
        }
        public Parameter Delete(int id)
        {
            Parameter data;
            HttpResponseMessage response = GlobalVariables.WebApiClient.DeleteAsync("Parameter/" + id).Result;
            data = response.Content.ReadAsAsync<Parameter>().Result;
            return data;
        }

    }
}