﻿using SalesMenClient.Models.Global;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Net.Http;
using System.Web;

namespace SalesMenClient.Models.Entity
{
    public class Order
    {
        public int ID { get; set; }
        public int AutionID { get; set; }
        //ten nguoi nhan
        public string Receiver { get; set; }
        public int CustomerID { get; set; }
        public int Payment { get; set; }
        public string Email { get; set; }
        public string Note { get; set; }
        public int status { get; set; }
        public string Phone { get; set; }
        public DateTime Created { get; set; }
        [ForeignKey("AutionID")]
        public Auction Auction { get; set; }
        public int ProAucId { get; set; }
        public float TotalPrice { get; set; }
        public float AuctionFees { get; set; }
        public float MoneyReceived { get; set; }
        public DateTime EndDateTime { get; set; }
        public IEnumerable<Order> GetAll()
        {
            IEnumerable<Order> data;
            HttpResponseMessage response = GlobalVariables.WebApiClient.GetAsync("Order").Result;
            data = response.Content.ReadAsAsync<IEnumerable<Order>>().Result;
            return data;

        }

        public Order GetById(int id)
        {
            Order data;
            HttpResponseMessage response = GlobalVariables.WebApiClient.GetAsync("Order/" + id).Result;
            data = response.Content.ReadAsAsync<Order>().Result;
            return data;
        }

        //lay tat ca don hang
        public List<Order> GetAllOrder()
        {
            List<Order> list = new List<Order>();
            var data = GetAll();
            Auction auc = new Auction();
            foreach(var item in data)
            {
                Auction auData = auc.GetById(item.AutionID);
                item.Auction = auData;
                item.ProAucId = auData.ProAutionID;
                list.Add(item);
            }
            return list;
        }

        public Order GetByProID (int proAucId)
        {
            var data = GetAllOrder();
            return data.Find(x=>x.ProAucId==proAucId);

        }
        public Order GetByOrderID(int id)
        {
            var data = GetAllOrder();
            return data.Find(x=>x.ID==id);
        }
        public Order GetOrderByAucID(int auc_id)
        {
            var data = GetAllOrder();
            return data.Find(x => x.AutionID == auc_id);
        }
        //status don hang
        public string OrderSt(int status)
        {
            var value = "";
            switch (status)
            {
                case 0:
                    //dang cho kh xac nhan
                    value = "Wait for confirmation";
                    break;
                case 1:
                    //dang cho kh giao hang
                    value = "Waiting for delivery";
                    break;
                case 2:
                    //Dang giao hang
                    value = "Being transported";
                    break;
                case 3:
                    //Da giao hang
                    value = "Complete";
                    break;
                case 4:
                    //bi huy
                    value = "Cancel";
                    break;
                default:
                    value = "chua xac dinh";
                    break;
            }
            return value;
        }
        //class status don hang
        public string OrderStClass(int status)
        {
            var value = "";
            switch (status)
            {
                case 0:
                    //dang cho kh xac nhan
                    value = "badge badge-secondary";
                    break;
                case 1:
                    //dang cho kh giao hang
                    value = "badge badge-warning";
                    break;
                case 2:
                    //Dang giao hang
                    value = "badge badge-info";
                    break;
                case 3:
                    //Da giao hang
                    value = "badge badge-success";
                    break;
                case 4:
                    //bi huy
                    value = "badge badge-danger";
                    break;
                default:
                    value = "badge badge-primary";
                    break;
            }
            return value;
        }

        //count theo status 
        public int CountOrder(int status)
        {
            var data = GetAll().Where(x => x.status == status);
            return data.Count();
        }

    }
}