﻿using SalesMenClient.Models.Global;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Net.Http;
using System.Web;

namespace SalesMenClient.Models.Entity
{
    public class Auction
    {
        public int ID { get; set; }
        public int ProAutionID { get; set; }
        public int CustomerID { get; set; }
        public float Price { get; set; }
        public int? Status { get; set; }
        public DateTime Created { get; set; }
        [ForeignKey("ProAutionID")]
        public ProductAuction ProductAuction { get; set; }
        [ForeignKey("CustomerID")]
        public Customer Customer { get; set; }
        public IEnumerable<Order> Orders { get; set; }

        public IEnumerable<Auction> GetAll()
        {
            IEnumerable<Auction> data;
            HttpResponseMessage response = GlobalVariables.WebApiClient.GetAsync("Auction").Result;
            data = response.Content.ReadAsAsync<IEnumerable<Auction>>().Result;
            return data;

        }

        public Auction GetById(int id)
        {
            Auction data;
            HttpResponseMessage response = GlobalVariables.WebApiClient.GetAsync("Auction/" + id).Result;
            data = response.Content.ReadAsAsync<Auction>().Result;
            return data;
        }

        public IEnumerable<Auction> GetByProduct(int proAuctionID)
        {
            IEnumerable<Auction> data;
            HttpResponseMessage response = GlobalVariables.WebApiClient.GetAsync("Auction?proAuctionID=" + proAuctionID).Result;
            data = response.Content.ReadAsAsync<IEnumerable<Auction>>().Result;
            return data;
        }

        public Auction Edit(Auction c)
        {
            Auction data;
            HttpResponseMessage response = GlobalVariables.WebApiClient.PutAsJsonAsync("Auction", c).Result;
            data = response.Content.ReadAsAsync<Auction>().Result;
            return data;
        }

        public Auction AddNew(Auction c)
        {
            Auction data;
            HttpResponseMessage response = GlobalVariables.WebApiClient.PostAsJsonAsync("Auction", c).Result;
            data = response.Content.ReadAsAsync<Auction>().Result;
            return data;
        }
        public Auction Delete(int id)
        {
            Auction data;
            HttpResponseMessage response = GlobalVariables.WebApiClient.DeleteAsync("Auction/" + id).Result;
            data = response.Content.ReadAsAsync<Auction>().Result;
            return data;
        }
        public Auction AucPriceDESC(int proAuctionID)
        {
            var data = GetByProduct(proAuctionID);
         
            if (data.Count()>0)
            {
                return data.OrderByDescending(x => x.Created).First();
            }
            else
            {
                return null;
            } 
        }

        public int AuctionGetProductID(int proID)
        {
            var data = GetByProduct(proID);

            if (data.Count() > 0)
            {
                return data.Count();
            }
            else
            {
                return 0;
            }
        }

        public List<Auction> GetList(int id)
        {
            Customer c = new Customer();
            List<Auction> list = new List<Auction>();
            var data = GetByProduct(id);

            foreach (var item in data)
            {
                item.Customer = c.GetById(item.CustomerID);

                list.Add(item);
            }
            return list;
        }

        public List<Auction> GetListAllBySale(int saleManId)
        {
            ProductAuction p = new ProductAuction();
            List<Auction> list = new List<Auction>();
            Customer c = new Customer();

            var dataProSm = p.GetBySaleMan(saleManId);
            foreach(var pro in dataProSm)
            {
                var dataAu = GetByProduct(pro.ID);
                foreach(var item in dataAu)
                {
                    item.Customer = c.GetById(item.CustomerID);
                    item.ProductAuction = p.GetById(pro.ID);
                    list.Add(item);
                }
            }


            return list;
        }

    }
}