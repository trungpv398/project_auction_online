﻿using SalesMenClient.Models.Global;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Net.Http;
using System.Web;

namespace SalesMenClient.Models.Entity
{
    public class ProductAuction
    {

        public int ID { get; set; }
        public string ProName { get; set; }
        // noi san xuat
        public string Origin { get; set; }
        //gia goc
        public float OriginalPrice { get; set; }
        //gia buoc nhay
        public float PriceStep { get; set; }
        //gia mua ngay
        public float PurchasingPrice { get; set; }
        //gia khoi diem
        public float StartingPrice { get; set; }
        //mo ta san pham
        public string Description { get; set; }
        //danh gia
        public int Rated { get; set; }
        public string Image { get; set; }
        public DateTime StartDateTime { get; set; }
        public DateTime EndDateTime { get; set; }
        public int Qty { get; set; }
        public int Status { get; set; }
        public int CategoryID { get; set; }
        public int SaleManID { get; set; }
        public int BrandID { get; set; }
        public DateTime Created { get; set; }
        public DateTime Updated { get; set; }
        [ForeignKey("CategoryID")]
        public Category Category { get; set; }
        [ForeignKey("SaleManID")]
        public SalesMan SalesMan { get; set; }
        [ForeignKey("BrandID")]
        public Brand Brand { get; set; }
        public IEnumerable<ProAuParameter> ProAuParameters { get; set; }
        public IEnumerable<Auction> Actions { get; set; }


        public IEnumerable<ProductAuction> GetAll()
        {
            IEnumerable<ProductAuction> data;
            HttpResponseMessage response = GlobalVariables.WebApiClient.GetAsync("ProductAuction").Result;
            data = response.Content.ReadAsAsync<IEnumerable<ProductAuction>>().Result;
            return data;

        }

        public ProductAuction GetById(int id)
        {
            ProductAuction data;
            HttpResponseMessage response = GlobalVariables.WebApiClient.GetAsync("ProductAuction/" + id).Result;
            data = response.Content.ReadAsAsync<ProductAuction>().Result;
            return data;
        }

        public IEnumerable<ProductAuction> GetBySaleMan(int salemanId)
        {
            IEnumerable<ProductAuction> data;
            HttpResponseMessage response = GlobalVariables.WebApiClient.GetAsync("ProductAuction?salemanId="+salemanId).Result;
            data = response.Content.ReadAsAsync<IEnumerable<ProductAuction>>().Result;
            return data;
        }

        public ProductAuction Edit(ProductAuction c)
        {
            ProductAuction data;
            HttpResponseMessage response = GlobalVariables.WebApiClient.PutAsJsonAsync("ProductAuction", c).Result;
            data = response.Content.ReadAsAsync<ProductAuction>().Result;
            return data;
        }

        public ProductAuction AddNew(ProductAuction c)
        {
            ProductAuction data;
            HttpResponseMessage response = GlobalVariables.WebApiClient.PostAsJsonAsync("ProductAuction", c).Result;
            data = response.Content.ReadAsAsync<ProductAuction>().Result;
            return data;
        }
        public ProductAuction Delete(int id)
        {
            ProductAuction data;
            HttpResponseMessage response = GlobalVariables.WebApiClient.DeleteAsync("ProductAuction/" + id).Result;
            data = response.Content.ReadAsAsync<ProductAuction>().Result;
            return data;
        }

        public ProductAuction GetProById(int id)
        {
            Brand brand = new Brand();
            Category cate = new Category();
            ProductAuction proAu = GetById(id);
            proAu.Brand = brand.GetById(proAu.BrandID);
            proAu.Category = cate.GetById(proAu.CategoryID);

            return proAu;
        }

        public void UpProSt()
        {
            var time = DateTime.Now;
            var data = GetAll().Where(x => x.Status == 0);
            foreach(var item in data)
            {
                if(item.StartDateTime <=time)
                {
                    UpdateStatus(item.ID);
                }
            }
        }

        public void UpdateStatus(int proId)
        {
            var data = GetById(proId);
            data.Status = 1;
            Edit(data);
        }
       

    }
}