﻿using SalesMenClient.Models.Global;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Net.Http;
using System.Web;

namespace SalesMenClient.Models.Entity
{
    public class Banner
    {
        public int ID { get; set; }
        public int TypeBannerID { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Image { get; set; }
        public int Status { get; set; }
        public DateTime Created { get; set; }
     

        public IEnumerable<Banner> GetAll()
        {
            IEnumerable<Banner> data;
            HttpResponseMessage response = GlobalVariables.WebApiClient.GetAsync("Banner").Result;
            data = response.Content.ReadAsAsync<IEnumerable<Banner>>().Result;
            return data;

        }

        public Banner GetById(int id)
        {
            Banner data;
            HttpResponseMessage response = GlobalVariables.WebApiClient.GetAsync("Banner/" + id).Result;
            data = response.Content.ReadAsAsync<Banner>().Result;
            return data;
        }

        public Banner Edit(Banner c)
        {
            Banner data;
            HttpResponseMessage response = GlobalVariables.WebApiClient.PutAsJsonAsync("Banner", c).Result;
            data = response.Content.ReadAsAsync<Banner>().Result;
            return data;
        }

        public Message AddNew(Banner c)
        {
            Message data;
            HttpResponseMessage response = GlobalVariables.WebApiClient.PostAsJsonAsync("Banner", c).Result;
            data = response.Content.ReadAsAsync<Message>().Result;
            return data;
        }
        public Banner Delete(int id)
        {
            Banner data;
            HttpResponseMessage response = GlobalVariables.WebApiClient.DeleteAsync("Banner/" + id).Result;
            data = response.Content.ReadAsAsync<Banner>().Result;
            return data;
        }

    }
}