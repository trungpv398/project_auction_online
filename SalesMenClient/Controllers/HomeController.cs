﻿using CloudinaryDotNet;
using CloudinaryDotNet.Actions;
using PagedList;
using SalesMenClient.Models.Entity;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace SalesMenClient.Controllers
{
    public class HomeController : Controller
    {
        SalesMan saleman;
        Category category;
        Brand brand;
        Type_Product type_Product;
        Parameter parameter;
        ProductAuction productAuction;
        ProAuParameter proAuParameter;
        Auction auction;
        ProductOrder proOrder;
        Order order;
        private UserManuals userManuals;
        private CustomerSuport customerSuport;
        public static Cloudinary cloudinary;
        public const string CLOUD_NAME = "dk65dthn5";
        public const string API_KEY = "887159183214426";
        public const string API_SECRET = "4ihijjYSSMO6hBARbJ05lb4k8Ek";
        Account account = new Account(CLOUD_NAME, API_KEY, API_SECRET);
        public HomeController()
        {
            saleman         = new SalesMan();
            category        = new Category();
            brand           = new Brand();
            type_Product    = new Type_Product();
            parameter       = new Parameter();
            productAuction  = new ProductAuction();
            proAuParameter  = new ProAuParameter();
            auction         = new Auction();
            proOrder        = new ProductOrder();
            order           = new Order();
            userManuals     = new UserManuals();
            customerSuport  = new CustomerSuport();
        }
        public ActionResult Index(int? page)
        {
            if (Session["loginSaleman"] == null)
            {
                return RedirectToAction("Login", "Home");
            }
            int _page = page ?? 1;
            int _pageSize = 20;
            var time = DateTime.Now;
            productAuction.UpProSt();
            var dataPro = productAuction.GetAll().Where(x => x.Status == 1);
            foreach(var item in dataPro)
            {
                if(item.EndDateTime <= time)
                {
                    UpdateStatusPro(item.ID);
                }
            }

            var dataAutionAll = auction.GetListAllBySale((Session["loginSaleman"]as SalesMan).ID);
            ViewBag.AutionAll = dataAutionAll.OrderByDescending(x => x.Created).ToPagedList(_page, _pageSize);
            var dataOrder = proOrder.GetListPOBySalesMan((Session["loginSaleman"] as SalesMan).ID).OrderBy(x => x.Order.Created);
            ViewBag.OrderAll = dataOrder.ToPagedList(_page, _pageSize);

            var dataProUpcomming = productAuction.GetBySaleMan((Session["loginSaleman"] as SalesMan).ID).Where(x => x.Status == 1);
            ViewBag.ProUpcomming = dataProUpcomming.Count();
            ViewBag.OrderSt = dataOrder.Where(x => x.Order.status == 1).Count();
            return View();
        }


        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }


        public ActionResult Register()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Register(FormCollection form)
        {
            try
            {
                SalesMan s = new SalesMan();
                s.Email = form["email"];
                s.FullName = form["fullname"];
                s.PhoneNumber = form["phonenumber"];
                s.CMND = form["cmnd"];
                s.Address = form["address"];
                s.Password = GETSHA(form["password"]);
                s.Created = DateTime.Now;
                s.Status = 1;
                s.Rate = 0;
                 var data = saleman.AddNew(s);
                if(data.StatusCode==200)
                {
                    TempData["Success"] = "Successfully register..!";
                    return RedirectToAction("Login");
                }
                else
                {
                    TempData["Errors"] = "Register failed..!";
                    return RedirectToAction("Login");

                }
            }
            catch
            {
                TempData["Errors"] = "Register failed1..!";
                return RedirectToAction("Register");
            }
        }


        public ActionResult Login()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Login(FormCollection form)
        {
            var data = saleman.GetAll();
            foreach(var item in data)
            {
                if(item.Email == form["email"] && item.Password == GETSHA(form["password"]))
                {
                    SalesMan s = item;
                    Session["loginSaleman"] = s;
                    return RedirectToAction("Index");
                }
            }
            return RedirectToAction("Login");
        }
        public ActionResult Logout()
        {
            Session["loginSaleman"] = null;
            return RedirectToAction("Index");
        }
        
        //quan tri don hang
        public ActionResult MyOrder(int id,int? page)
        {
            if (Session["loginSaleman"] == null)
            {
                return RedirectToAction("Login", "Home");
            }
            int _page = page ?? 1;
            int _pageSize = 20;
            var data = proOrder.GetListPOBySalesMan(id).OrderBy(x => x.Order.Created);
            ViewBag.OrderAll = data.ToPagedList(_page,_pageSize);
            ViewBag.Order0 = data.Where(x => x.Order.status == 0).ToPagedList(_page, _pageSize);
            ViewBag.Order1 = data.Where(x => x.Order.status == 1).ToPagedList(_page, _pageSize);
            ViewBag.Order2 = data.Where(x => x.Order.status == 2).ToPagedList(_page, _pageSize);
            ViewBag.Order3 = data.Where(x => x.Order.status == 3).ToPagedList(_page, _pageSize);
            ViewBag.Order4 = data.Where(x => x.Order.status == 4).ToPagedList(_page, _pageSize);

            ViewBag.Order0Count = data.Where(x => x.Order.status == 0).Count();
            ViewBag.Order1Count = data.Where(x => x.Order.status == 1).Count();
            ViewBag.Order2Count = data.Where(x => x.Order.status == 2).Count();
            ViewBag.Order3Count = data.Where(x => x.Order.status == 3).Count();
            return View();
        }
        //chi tiet don hang
        public ActionResult OrderDetail(int id)
        {
            var data = order.GetByOrderID(id);
            ViewBag.Detail = data;
            return PartialView("_OrderDetail");
        }

        public ActionResult MyBids(int id)
        {
            ViewBag.Category = category.GetAll().Where(x =>x.Status ==1);
            ViewBag.Brand = brand.GetAll().Where(x => x.Status == 1);
            ViewBag.TypePro = type_Product.GetAll().Where(x => x.Status == 1);
            ViewBag.ProUpcomming = productAuction.GetBySaleMan(id).Where(x => x.Status==1);
            ViewBag.dataAuction = auction.GetAll().OrderByDescending(x=>x.Price);

            //san phẩm đã hết giờ
            ViewBag.ProductPass = productAuction.GetBySaleMan(id).Where(x => x.Status == 2||x.Status==3||x.Status==4||x.Status==5);
            ViewBag.ProUpcoming = productAuction.GetBySaleMan(id).Where(x => x.Status == 0);

            return PartialView("MyBids");
        }
        public ActionResult PersonalProfile(int id)
        {
            ViewBag.profile = saleman.GetById(id);
            return PartialView("_PersonalProfile");
        }
        public ActionResult AddProduct(int id)
        {
            if (Session["loginSaleman"] == null)
            {
                return RedirectToAction("Login", "Home");
            }
            ViewBag.Category = category.GetAll().Where(x => x.Status == 1);
            ViewBag.Brand = brand.GetAll().Where(x => x.Status == 1);
            ViewBag.TypePro = type_Product.GetAll().Where(x => x.Status == 1);
            return View();
        }

        public ActionResult GetPramByID(int type_id)
        {
            ViewBag.Parameter = parameter.GetByType(type_id).Where(x=>x.Status==1);
            ViewBag.Brand = brand.GetAll().Where(x => x.TypeProID == type_id).Where(x => x.Status == 1);
            return PartialView("_GetPramByID");
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult AddProduct(FormCollection form,string[] param_id,string[] value_param,string[] param_name, HttpPostedFileBase file)
        {
            if (Session["loginSaleman"] == null)
            {
                return RedirectToAction("Login", "Home");
            }
            ProductAuction proAu = new ProductAuction();
            cloudinary = new Cloudinary(account);
            string _File = "";
            string _FileName = "";
            string _path = "";
            try
            {
                DateTime startDT = DateTime.Parse(form["datestart"] + " " + form["timestart"]);
                DateTime endDT = DateTime.Parse(form["dateend"] + " " + form["timeend"]);

                if(startDT>=endDT)
                {
                    TempData["Errors"] = "The starting time must be less than the end time..!";
                    return RedirectToAction("AddProduct");
                }

            if (file.ContentLength > 0)
            {
                _File = Path.GetFileName(file.FileName);
                _FileName = _File;
                _path = Path.Combine(Server.MapPath("~/Assets/uploads"), _FileName);
                file.SaveAs(_path);
                var uploadParams = new ImageUploadParams()
                {
                    File = new FileDescription(_path),

                };
                var uploadResult = cloudinary.Upload(uploadParams);
                proAu.Image = "https://res.cloudinary.com/dk65dthn5/" + uploadResult.FullyQualifiedPublicId;
                System.IO.File.Delete(_path);
            }
            proAu.Created = DateTime.Now;
            proAu.Updated = DateTime.Now;
            proAu.ProName = form["proName"];
            proAu.Origin = form["origin"];
            proAu.OriginalPrice = float.Parse(form["originalPrice"]);
            proAu.PriceStep = float.Parse(form["priceStep"]);
            proAu.PurchasingPrice = float.Parse(form["purchasingPrice"]);
            proAu.StartDateTime = startDT;
            proAu.EndDateTime = endDT;
            proAu.Qty = 1;
            proAu.Rated = 0;
            proAu.CategoryID = int.Parse(form["categoryid"]);
            proAu.SaleManID = (Session["loginSaleman"] as SalesMan).ID;
            proAu.BrandID = int.Parse(form["brandid"]);
            if (proAu.StartDateTime>proAu.Created)
            {
                proAu.Status = 0;

            }
            else
            {
                proAu.Status = 1;
            }
              
            proAu.Description = form["description"];
            proAu.StartingPrice = float.Parse(form["startingprice"]);
            var data = productAuction.AddNew(proAu);
            

                for (int i = 0; i < param_id.Length; i++)
                {
                    ProAuParameter pp = new ProAuParameter();
                    pp.ParameterID = int.Parse(param_id[i]);
                    pp.Value = value_param[i];
                    pp.ProAuctionID = data.ID;
                    pp.ParameterName = param_name[i];
                    pp.Created = DateTime.Now;
                    proAuParameter.AddNew(pp);
                }

                var dataSaleman = saleman.GetById((Session["loginSaleman"] as SalesMan).ID);
                dataSaleman.Rate = dataSaleman.Rate + 10;
                saleman.Edit(dataSaleman);


                TempData["Success"] = "Successfully added..!";
                return RedirectToAction("Index");
            }
            catch
            {

                return RedirectToAction("Index");
            }


        }

        public ActionResult EditProduct(int id)
        {
            var data = productAuction.GetProById(id);
            ViewBag.Data = data;
            return View();
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult EditProduct(FormCollection form)
        {
            var data = productAuction.GetProById(int.Parse(form["id"]));
            if(data.Status==0)
            {

                data.ProName = form["proName"];
                data.Origin = form["origin"];
                data.OriginalPrice = float.Parse(form["originalPrice"]);
                data.PriceStep = float.Parse(form["priceStep"]);
                data.PurchasingPrice = float.Parse(form["purchasingPrice"]);
                data.StartingPrice = float.Parse(form["startingprice"]);
                data.Description = form["description"];
                productAuction.Edit(data);
                TempData["Success"] = "Successfully update..!";
                return RedirectToAction("Index");
            }
            TempData["Errors"] = "Update failed..!";
            return RedirectToAction("Index");
        }

        public ActionResult DeleteProduct(int id)
        {
            var data = productAuction.GetProById(id);
            var dataParameter = proAuParameter.GetProAu(data.ID);
            foreach(var item in dataParameter)
            {
                proAuParameter.Delete(item.ID);
            }
            productAuction.Delete(id);
            var dataSaleman = saleman.GetById((Session["loginSaleman"] as SalesMan).ID);
            dataSaleman.Rate = dataSaleman.Rate - 5;
            saleman.Edit(dataSaleman);
            TempData["Success"] = "Successfully delete..!";
            return RedirectToAction("Index");
        }


        public ActionResult UserManuals(int id)
        {
            var data = userManuals.GetById(id);
            ViewBag.Data = data;
            return View();
        }
        public ActionResult Support(int id)
        {
            var data = customerSuport.GetById(id);
            ViewBag.Data = data;
            return View();
        }
        public void UpdateStatusPro(int proId)
        {
            var data = productAuction.GetById(proId);
            data.Status = 2;

            productAuction.Edit(data);

        }
        public static string GETSHA(string str)
        {
            /*MD5 md5 = new MD5CryptoServiceProvider();*/
            SHA512 sha = new SHA512CryptoServiceProvider();
            byte[] fromData = Encoding.UTF8.GetBytes(str);
            byte[] targetData = sha.ComputeHash(fromData);
            string byte2String = null;

            for (int i = 0; i < targetData.Length; i++)
            {
                byte2String += targetData[i].ToString("x2");

            }
            return byte2String;
        }


        /* var param = parameter.GetAll().Where(x=>x.TypeProID== int.Parse(form["type_id"]));
           foreach(Parameter item in param)
           {
               ProAuParameter pp = new ProAuParameter();
               pp.ParameterID = int.Parse(form["id_" + item.ParameterName]);
               pp.ParameterName = item.ParameterName;
               pp.Value = form["value_"+item.ParameterName];
               paramAuList.Add(pp);
           }    */
    }
}