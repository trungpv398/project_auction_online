﻿using SalesMenClient.Models.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SalesMenClient.Controllers
{
    public class ProductAutionController : Controller
    {

        ProductAuction productAuction;
        Auction auction;
        ProAuParameter proAuParameter;
        Brand brand;
        Order order;

        public ProductAutionController()
        {
            productAuction  = new ProductAuction();
            auction         = new Auction();
            proAuParameter  = new ProAuParameter();
            brand           = new Brand();
            order           = new Order();
        }
        // GET: ProductAution
        public ActionResult Detail(int id)
        {
            if (Session["loginSaleman"] == null)
            {
                return RedirectToAction("Login", "Home");
            }
            var data = productAuction.GetById(id);
            ViewBag.Brand = brand.GetById(data.BrandID);
            ViewBag.DetailProAuction = data;
            var dataAuction = auction.GetByProduct(id);
            if (dataAuction.Count() == 0)
            {
                ViewBag.ProAuction = null;
            }
            else
            {
                ViewBag.ProAuction = dataAuction.OrderByDescending(x => x.Created).First();


            }
            var dataParamPro = proAuParameter.GetProAu(id);
            ViewBag.Parameter = dataParamPro;
            var dataAuctionAll = auction.GetList(id).OrderByDescending(x => x.Created);
            ViewBag.ProAuctionAll = dataAuctionAll;
            ViewBag.TotalAution = dataAuctionAll.Count();

            return View();
        }
        public ActionResult CheckOrder(int id)
        {
            ViewBag.Detail = order.GetOrderByAucID(id);
            return PartialView("CheckOrder");
        }
    }
}