﻿using AdminOnlineAuction.Models.Global;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Net.Http;
using System.Web;

namespace AdminOnlineAuction.Models.Entity
{
    public class Order
    {

        public int ID { get; set; }
        public int AutionID { get; set; }
        //ten nguoi nhan
        public string Receiver { get; set; }
        public int CustomerID { get; set; }
        public int Payment { get; set; }
        public string Email { get; set; }
        public string Note { get; set; }
        public int status { get; set; }
        public string Phone { get; set; }
        public DateTime EndDateTime { get; set; }
        public DateTime Created { get; set; }
        [ForeignKey("AutionID")]
        public Auction Auction { get; set; }
        public Customer Customer { get; set; }
        public float TotalPrice { get; set; }
        public float AuctionFees { get; set; }
        public float MoneyReceived { get; set; }
        public int? ProAutionID { get; set; }

        public Message AddNew(Order c)
        {
            Message data;
            HttpResponseMessage response = GlobalVariables.WebApiClient.PostAsJsonAsync("Order", c).Result;
            data = response.Content.ReadAsAsync<Message>().Result;
            return data;
        }
        public IEnumerable<Order> GetAll()
        {
            IEnumerable<Order> data;
            HttpResponseMessage response = GlobalVariables.WebApiClient.GetAsync("Order").Result;
            data = response.Content.ReadAsAsync<IEnumerable<Order>>().Result;
            return data;

        }
        public Order EditOrder(Order c)
        {
            Order cate;
            HttpResponseMessage response = GlobalVariables.WebApiClient.PutAsJsonAsync("Order", c).Result;
            cate = response.Content.ReadAsAsync<Order>().Result;
            return cate;
        }

        public Order GetById(int id)
        {
            Order data;
            HttpResponseMessage response = GlobalVariables.WebApiClient.GetAsync("Order/" + id).Result;
            data = response.Content.ReadAsAsync<Order>().Result;
            return data;
        }

        //lay tat ca don hang
        public List<Order> GetOrderAll()
        {
            List<Order> list = new List<Order>();
            Auction auction = new Auction();
            Customer customer = new Customer();
            var data = GetAll();
            foreach(var item in data)
            {
                item.Customer = customer.GetById(item.CustomerID);
                item.Auction = auction.GetById(item.AutionID);
                list.Add(item);
            }
            return list;

        }
        //lay don hang theo id
        public Order OrderDetail(int id)
        {
            Order od = new Order();
            Auction au = new Auction();
            Customer cus = new Customer();
            var data = GetById(id);
            data.Auction = au.GetAcByID(data.AutionID);
            data.Customer = cus.GetById(data.CustomerID);

            return data;
        }

        //lay don hang theo aution id
        public Order OrderByAutionID(int id)
        {
           
            var data = GetOrderAll();
           

            return data.Where(x=>x.AutionID==id).First();

        }

        //get order by status
        public List<Order> GetOdByStatus(int status)
        {
            var data = GetOrderAll();
            return data.Where(x=>x.status == status).ToList();
        }
        public float TotalAuctionFees(int status,int month)
        {
            float total = 0;
            var dataAution = GetOdByStatus(status).Where(x=>x.EndDateTime.Month==month);
            foreach(var item in dataAution)
            {
                total = total + item.AuctionFees;
            }
            return total;
        }
        public float TotalTotalPrice(int status, int month)
        {
            float total = 0;
            var dataAution = GetOdByStatus(status).Where(x => x.EndDateTime.Month == month);
            foreach (var item in dataAution)
            {
                total = total + item.TotalPrice;
            }
            return total;
        }
        public float TotalMoneyReceived(int status, int month)
        {
            float total = 0;
            var dataAution = GetOdByStatus(status).Where(x => x.EndDateTime.Month == month);
            foreach (var item in dataAution)
            {
                total = total + item.MoneyReceived;
            }
            return total;
        }
        public Auction GetAutionByProId(int auid)
        {
            ProductAuction p = new ProductAuction();
            Auction auction = new Auction();
            var data = auction.GetById(auid);
            data.ProductAuction = p.GetById(data.ProAutionID);

            return data;
        }

        //check don hang st la status ben order va stnew la status cho san pham
        public void CheckOrder(int st,int stNew)
        {
            
            var time = DateTime.Now;
            var data = GetOdByStatus(st);
            foreach(var item in data)
            {
                if(item.EndDateTime <= time && item.Receiver==null && item.Phone==null)
                {
                    ProductAuction p = new ProductAuction();
                    item.status = stNew;
                    
                    EditOrder(item);
                    var au = GetAutionByProId(item.AutionID);
                    p.UpdateStatus(au.ProAutionID, stNew);
                }
            }

        }

        //status don hang
        public string OrderSt(int status)
        {
            var value = "";
            switch (status)
            {
                case 0:
                    //dang cho kh xac nhan
                    value = "Wait for confirmation";
                    break;
                case 1:
                    //dang cho kh giao hang
                    value = "Waiting for delivery";
                    break;
                case 2:
                    //Dang giao hang
                    value = "Being transported";
                    break;
                case 3:
                    //Da giao hang
                    value = "Complete";
                    break;
                case 4:
                    //bi huy
                    value = "Cancel";
                    break;
                default:
                    value = "chua xac dinh";
                    break;
            }
            return value;
        }
        //class status don hang
        public string OrderStClass(int status)
        {
            var value = "";
            switch (status)
            {
                case 0:
                    //dang cho kh xac nhan
                    value = "badge badge-secondary";
                    break;
                case 1:
                    //dang cho kh giao hang
                    value = "badge badge-warning";
                    break;
                case 2:
                    //Dang giao hang
                    value = "badge badge-info";
                    break;
                case 3:
                    //Da giao hang
                    value = "badge badge-success";
                    break;
                case 4:
                    //bi huy
                    value = "badge badge-danger";
                    break;
                default:
                    value = "badge badge-primary";
                    break;
            }
            return value;
        }

        //count theo status 
        public int CountOrder(int status)
        {
            var data = GetAll().Where(x => x.status == status);
            return data.Count();
        }

       

    }
}