﻿using AdminOnlineAuction.Models.Global;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;

namespace AdminOnlineAuction.Models.Entity
{
    public class Customer
    {

        public int ID { get; set; }
        public string FullName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string Address { get; set; }
        public int Status { get; set; }
        public string CMND { get; set; }
        public string PhoneNumber { get; set; }
        public string Avatar { get; set; }
        public int? Rate { get; set; }
        public DateTime Created { get; set; }
        public IEnumerable<Auction> Auctions { get; set; }
        public IEnumerable<Customer> GetAll()
        {
            IEnumerable<Customer> data;
            HttpResponseMessage response = GlobalVariables.WebApiClient.GetAsync("Customer").Result;
            data = response.Content.ReadAsAsync<IEnumerable<Customer>>().Result;
            return data;

        }
        public Customer GetById(int id)
        {
            Customer data;
            HttpResponseMessage response = GlobalVariables.WebApiClient.GetAsync("Customer/" + id).Result;
            data = response.Content.ReadAsAsync<Customer>().Result;
            return data;
        }
    }

}