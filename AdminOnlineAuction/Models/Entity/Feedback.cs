﻿using AdminOnlineAuction.Models.Global;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;

namespace AdminOnlineAuction.Models.Entity
{
    public class Feedback
    {
        public int ID { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public string Content { get; set; }
        public int Status { get; set; }
        public DateTime Created { get; set; }
        public IEnumerable<Feedback> GetAll()
        {
            IEnumerable<Feedback> data;
            HttpResponseMessage response = GlobalVariables.WebApiClient.GetAsync("FeedBack").Result;
            data = response.Content.ReadAsAsync<IEnumerable<Feedback>>().Result;
            return data;
        }
        public Feedback GetById(int id)
        {
            Feedback data;
            HttpResponseMessage response = GlobalVariables.WebApiClient.GetAsync("FeedBack/" + id).Result;
            data = response.Content.ReadAsAsync<Feedback>().Result;
            return data;
        }
        public Feedback Edit(Feedback c)
        {
            Feedback data;
            HttpResponseMessage response = GlobalVariables.WebApiClient.PutAsJsonAsync("FeedBack", c).Result;
            data = response.Content.ReadAsAsync<Feedback>().Result;
            return data;
        }
    }
}