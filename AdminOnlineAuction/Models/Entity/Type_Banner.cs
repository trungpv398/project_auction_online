﻿using AdminOnlineAuction.Models.Global;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;

namespace AdminOnlineAuction.Models.Entity
{
    public class Type_Banner
    {
        public int ID { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public int Status { get; set; }
        public DateTime Created { get; set; }
        public IEnumerable<Banner> Banners { get; set; }

        public IEnumerable<Type_Banner> GetAll()
        {
            IEnumerable<Type_Banner> data;
            HttpResponseMessage response = GlobalVariables.WebApiClient.GetAsync("TypeBanner").Result;
            data = response.Content.ReadAsAsync<IEnumerable<Type_Banner>>().Result;
            return data;

        }

        public Type_Banner GetById(int id)
        {
            Type_Banner data;
            HttpResponseMessage response = GlobalVariables.WebApiClient.GetAsync("TypeBanner/" + id).Result;
            data = response.Content.ReadAsAsync<Type_Banner>().Result;
            return data;
        }

        public Type_Banner Edit(Type_Banner c)
        {
            Type_Banner data;
            HttpResponseMessage response = GlobalVariables.WebApiClient.PutAsJsonAsync("TypeBanner", c).Result;
            data = response.Content.ReadAsAsync<Type_Banner>().Result;
            return data;
        }

        public Message AddNew(Type_Banner c)
        {
            Message data;
            HttpResponseMessage response = GlobalVariables.WebApiClient.PostAsJsonAsync("TypeBanner", c).Result;
            data = response.Content.ReadAsAsync<Message>().Result;
            return data;
        }
        public Type_Banner Delete(int id)
        {
            Type_Banner data;
            HttpResponseMessage response = GlobalVariables.WebApiClient.DeleteAsync("TypeBanner/" + id).Result;
            data = response.Content.ReadAsAsync<Type_Banner>().Result;
            return data;
        }

    }
}