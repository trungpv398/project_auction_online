﻿using AdminOnlineAuction.Models.Global;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Net.Http;
using System.Web;

namespace AdminOnlineAuction.Models.Entity
{
    public class Auction
    {

        public int ID { get; set; }
        public int ProAutionID { get; set; }
        public int CustomerID { get; set; }
        [DisplayFormat(DataFormatString = "{0:N0}", ApplyFormatInEditMode = true)]
        public float Price { get; set; }
        public int? Status { get; set; }
        public DateTime Created { get; set; }
        [ForeignKey("ProAutionID")]
        public ProductAuction ProductAuction { get; set; }
        [ForeignKey("CustomerID")]
        public Customer Customer { get; set; }
        public IEnumerable<Order> Orders { get; set; }

        public IEnumerable<Auction> GetAll()
        {
            IEnumerable<Auction> data;
            HttpResponseMessage response = GlobalVariables.WebApiClient.GetAsync("Auction").Result;
            data = response.Content.ReadAsAsync<IEnumerable<Auction>>().Result;
            return data;
        }
        public Auction GetById(int id)
        {
            Auction data;
            HttpResponseMessage response = GlobalVariables.WebApiClient.GetAsync("Auction/" + id).Result;
            data = response.Content.ReadAsAsync<Auction>().Result;
            return data;
        }
        public IEnumerable<Auction> GetByProduct(int proAuctionID)
        {
            IEnumerable<Auction> data;
            HttpResponseMessage response = GlobalVariables.WebApiClient.GetAsync("Auction?proAuctionID=" + proAuctionID).Result;
            data = response.Content.ReadAsAsync<IEnumerable<Auction>>().Result;
            return data;
        }
        public Auction Edit(Auction c)
        {
            Auction data;
            HttpResponseMessage response = GlobalVariables.WebApiClient.PutAsJsonAsync("Auction", c).Result;
            data = response.Content.ReadAsAsync<Auction>().Result;
            return data;
        }
        public Auction AddNew(Auction c)
        {
            Auction data;
            HttpResponseMessage response = GlobalVariables.WebApiClient.PostAsJsonAsync("Auction", c).Result;
            data = response.Content.ReadAsAsync<Auction>().Result;
            return data;
        }
        public Auction Delete(int id)
        {
            Auction data;
            HttpResponseMessage response = GlobalVariables.WebApiClient.DeleteAsync("Auction/" + id).Result;
            data = response.Content.ReadAsAsync<Auction>().Result;
            return data;
        }
        //lay tat ca don đau gia theo san pham
        public List<Auction> GetListByPro(int proId)
        {
            Customer c = new Customer();
            List<Auction> list = new List<Auction>();
            var data = GetByProduct(proId);
            foreach(var item in data)
            {
                item.Customer = c.GetById(item.CustomerID);
                list.Add(item);
            }
            return list;
        }

        public Auction GetAcByID(int id)
        {
            var data = GetById(id);
            ProductAuction pro = new ProductAuction();
            Customer customer = new Customer();
            data.Customer = customer.GetById(data.CustomerID);
            data.ProductAuction = pro.GetById(data.ProAutionID);
            return data;
        }
        
        
    }
}