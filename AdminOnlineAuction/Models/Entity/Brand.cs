﻿using AdminOnlineAuction.Models.Global;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Net.Http;
using System.Web;

namespace AdminOnlineAuction.Models.Entity
{
    public class Brand
    {

        public int ID { get; set; }
        public string BrandName { get; set; }
        public string Image { get; set; }
        public int Status { get; set; }
        public int TypeProID { get; set; }
        public DateTime Created { get; set; }
        [ForeignKey("TypeProID")]
        public Type_Product Type_Product { get; set; }

        public IEnumerable<Brand> GetAll()
        {
            IEnumerable<Brand> data;
            HttpResponseMessage response = GlobalVariables.WebApiClient.GetAsync("Brand").Result;
            data = response.Content.ReadAsAsync<IEnumerable<Brand>>().Result;
            return data;

        }

        public Brand GetById(int id)
        {
            Brand data;
            HttpResponseMessage response = GlobalVariables.WebApiClient.GetAsync("Brand/" + id).Result;
            data = response.Content.ReadAsAsync<Brand>().Result;
            return data;
        }

        public Brand Edit(Brand c)
        {
            Brand data;
            HttpResponseMessage response = GlobalVariables.WebApiClient.PutAsJsonAsync("Brand", c).Result;
            data = response.Content.ReadAsAsync<Brand>().Result;
            return data;
        }

        public Message AddNew(Brand c)
        {
            Message data;
            HttpResponseMessage response = GlobalVariables.WebApiClient.PostAsJsonAsync("Brand", c).Result;
            data = response.Content.ReadAsAsync<Message>().Result;
            return data;
        }
        public Brand Delete(int id)
        {
            Brand data;
            HttpResponseMessage response = GlobalVariables.WebApiClient.DeleteAsync("Brand/" + id).Result;
            data = response.Content.ReadAsAsync<Brand>().Result;
            return data;
        }

    }
}