﻿using AdminOnlineAuction.Models.Global;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace AdminOnlineAuction.Models.Entity
{
    public class User
    {
        public int ID { get; set; }
        public string FullName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string Address { get; set; }
        public int Status { get; set; }
        public string CMND { get; set; }
        public string PhoneNumber { get; set; }
        public string Avatar { get; set; }

        public DateTime Created { get; set; }
        public IEnumerable<User> GetAll()
        {
            IEnumerable<User> data;
            HttpResponseMessage response = GlobalVariables.WebApiClient.GetAsync("User").Result;
            data = response.Content.ReadAsAsync<IEnumerable<User>>().Result;
            return data;

        }

        public User GetById(int id)
        {
            User data;
            HttpResponseMessage response = GlobalVariables.WebApiClient.GetAsync("User/" + id).Result;
            data = response.Content.ReadAsAsync<User>().Result;
            return data;
        }

        public User Edit(User c)
        {
            User data;
            HttpResponseMessage response = GlobalVariables.WebApiClient.PutAsJsonAsync("User", c).Result;
            data = response.Content.ReadAsAsync<User>().Result;
            return data;
        }

        public Message AddNew(User c)
        {
            Message data;
            HttpResponseMessage response = GlobalVariables.WebApiClient.PostAsJsonAsync("User", c).Result;
            data = response.Content.ReadAsAsync<Message>().Result;
            return data;
        }
        public User Delete(int id)
        {
            User data;
            HttpResponseMessage response = GlobalVariables.WebApiClient.DeleteAsync("User/" + id).Result;
            data = response.Content.ReadAsAsync<User>().Result;
            return data;
        }
        public  string GETSHA(string str)
        {
            /*MD5 md5 = new MD5CryptoServiceProvider();*/
            SHA512 sha = new SHA512CryptoServiceProvider();
            byte[] fromData = Encoding.UTF8.GetBytes(str);
            byte[] targetData = sha.ComputeHash(fromData);
            string byte2String = null;

            for (int i = 0; i < targetData.Length; i++)
            {
                byte2String += targetData[i].ToString("x2");

            }
            return byte2String;
        }
    }
}