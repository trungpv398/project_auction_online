﻿using AdminOnlineAuction.Models.Global;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Net.Http;
using System.Web;

namespace AdminOnlineAuction.Models.Entity
{
    public class ProductAuction
    {

        public int ID { get; set; }
        public string ProName { get; set; }
        // noi san xuat
        public string Origin { get; set; }
        //gia goc
        [DisplayFormat(DataFormatString = "{0:N0}", ApplyFormatInEditMode = true)]
        public float OriginalPrice { get; set; }
        //gia buoc nhay
        [DisplayFormat(DataFormatString = "{0:N0}", ApplyFormatInEditMode = true)]
        public float PriceStep { get; set; }
        //gia mua ngay
        [DisplayFormat(DataFormatString = "{0:N0}", ApplyFormatInEditMode = true)]
        public float PurchasingPrice { get; set; }
        //mo ta san pham
        public string Description { get; set; }
        public int Status { get; set; }
        public string Image { get; set; }
        public DateTime StartDateTime { get; set; }
        public DateTime EndDateTime { get; set; }
        public int Qty { get; set; }
        public int CategoryID { get; set; }
        public int SaleManID { get; set; }
        public int BrandID { get; set; }
        public DateTime Created { get; set; }
        public DateTime Updated { get; set; }
        [ForeignKey("CategoryID")]
        public Category Category { get; set; }
        [ForeignKey("SaleManID")]
        public SalesMan SalesMan { get; set; }
        [ForeignKey("BrandID")]
        public Brand Brand { get; set; }
        public IEnumerable<ProAuParameter> ProAuParameters { get; set; }
        public IEnumerable<Action> Actions { get; set; }


        public IEnumerable<ProductAuction> GetAll()
        {
            IEnumerable<ProductAuction> data;
            HttpResponseMessage response = GlobalVariables.WebApiClient.GetAsync("ProductAuction").Result;
            data = response.Content.ReadAsAsync<IEnumerable<ProductAuction>>().Result;
            return data;

        }
        public ProductAuction GetById(int id)
        {
            ProductAuction data;
            HttpResponseMessage response = GlobalVariables.WebApiClient.GetAsync("ProductAuction/" + id).Result;
            data = response.Content.ReadAsAsync<ProductAuction>().Result;
            return data;
        }
        public IEnumerable<ProductAuction> GetBySaleMan(int salemanId)
        {
            IEnumerable<ProductAuction> data;
            HttpResponseMessage response = GlobalVariables.WebApiClient.GetAsync("ProductAuction?salemanId=" + salemanId).Result;
            data = response.Content.ReadAsAsync<IEnumerable<ProductAuction>>().Result;
            return data;
        }
        public ProductAuction Edit(ProductAuction c)
        {
            ProductAuction data;
            HttpResponseMessage response = GlobalVariables.WebApiClient.PutAsJsonAsync("ProductAuction", c).Result;
            data = response.Content.ReadAsAsync<ProductAuction>().Result;
            return data;
        }
        public ProductAuction AddNew(ProductAuction c)
        {
            ProductAuction data;
            HttpResponseMessage response = GlobalVariables.WebApiClient.PostAsJsonAsync("ProductAuction", c).Result;
            data = response.Content.ReadAsAsync<ProductAuction>().Result;
            return data;
        }
        public ProductAuction Delete(int id)
        {
            ProductAuction data;
            HttpResponseMessage response = GlobalVariables.WebApiClient.DeleteAsync("ProductAuction/" + id).Result;
            data = response.Content.ReadAsAsync<ProductAuction>().Result;
            return data;
        }

        public ProductAuction ProductByID(int id)
        {
            var pro = GetById(id);
            Category cate = new Category();
            var dataCate = cate.GetAll();
            foreach(var item in dataCate)
            {
                if(item.ID == pro.CategoryID)
                {
                    pro.Category = item;
                }
            }
            return pro;
            
        }

        public List<ProductAuction> GetProAll()
        {
            Category cate = new Category();
            SalesMan sale = new SalesMan();
            Brand brand = new Brand();
            List<ProductAuction> list = new List<ProductAuction>();
            var data = GetAll();
            foreach(var item in data)
            {
                item.Category = cate.GetById(item.CategoryID);
                item.SalesMan = sale.GetById(item.SaleManID);
                item.Brand = brand.GetById(item.BrandID);
                list.Add(item);
            }
            return list;

        }

        public ProductAuction GetProByID(int id)
        {
            Category cate = new Category();
            SalesMan sale = new SalesMan();
            Brand brand = new Brand();
            var data = GetById(id);
            data.Category = cate.GetById(data.CategoryID);
            data.Brand = brand.GetById(data.BrandID);
            data.SalesMan = sale.GetById(data.SaleManID);
            return data;

        }

        //goi ra tat ca cac do hang thanh cong roi cap nhat trang thai san pham do thanh cong
        public void UpdateStSuccessPro(int st)
        {
            Order order = new Order();
            //lay ra tat ca don hang o trang thai success
            var dataOrder = order.GetOdByStatus(st);
            foreach(var item in dataOrder)
            {
                var au = order.GetAutionByProId(item.AutionID);
                UpdateStatus(au.ProAutionID,st);
            }

        }

        //cap nhat trang thai
        public void UpdateStatus(int id,int status)
        {
            var data = GetById(id);
            data.Status = status;
            Edit(data);
        }


        public string ProAuStatus(int status)
        {
            string value = "";
            switch (status)
            {
                case 0:
                    value = "Upcoming";
                    break;
                case 1:
                    value = "Are active";
                    break;
                case 2:
                    value = "End of the auction";
                    break;
                case 3:
                    value = "Successful auction";
                    break;
                case 4:
                    value = "The auction failed";
                    break;
                case 5:
                    value = "Buy now";
                    break;
                default:
                    value = "The auction failed";
                    break;
            }

            return value;
        }
        public string ClassStatus(int status)
        {
            string value = "";
            switch (status)
            {
                case 0:
                    value = "badge badge-secondary";
                    break;
                case 1:
                    value = "badge badge-info";
                    break;
                case 2:
                    value = "badge badge-warning";
                    break;
                case 3:
                    value = "badge badge-success";
                    break;
                case 4:
                    value = "badge badge-danger";
                    break;
                case 5:
                    value = "badge badge-success";
                    break;
                default:
                    value = "badge badge-info";
                    break;
            }

            return value;
        }

        public int CoutPro(int status)
        {
            var data = GetAll().Where(x => x.Status == status);
            return data.Count();
        }

        //check selected
        public string CheckSelected(int a,int b)
        {
            if(a==b)
            {
                return "selected";
            }
            else
            {
                return "";
            }

        }
    }
}