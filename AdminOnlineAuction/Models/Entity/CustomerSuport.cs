﻿using AdminOnlineAuction.Models.Global;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net.Http;
using System.Web;

namespace AdminOnlineAuction.Models.Entity
{
    public class CustomerSuport
    {

        public int ID { get; set; }
        [Required(ErrorMessage ="Khong duoc de trong title")]
        public string Title { get; set; }
        [Required(ErrorMessage = "Khong duoc de trong content")]
        public string Content { get; set; }
        public int Status { get; set; }
        public DateTime Created { get; set; }

        //Get Put Post Delete Api
        public IEnumerable<CustomerSuport> GetAll()
        {
            IEnumerable<CustomerSuport> supList;
            HttpResponseMessage response = GlobalVariables.WebApiClient.GetAsync("Support").Result;
            supList = response.Content.ReadAsAsync<IEnumerable<CustomerSuport>>().Result;
            return supList;

        }

        public CustomerSuport GetById(int id)
        {
            CustomerSuport cusSup;
            HttpResponseMessage response = GlobalVariables.WebApiClient.GetAsync("Support/" + id).Result;
            cusSup = response.Content.ReadAsAsync<CustomerSuport>().Result;
            return cusSup;
        }

        public CustomerSuport Edit(CustomerSuport c)
        {
            CustomerSuport cusSup;
            HttpResponseMessage response = GlobalVariables.WebApiClient.PutAsJsonAsync("Support", c).Result;
            cusSup = response.Content.ReadAsAsync<CustomerSuport>().Result;
            return cusSup;
        }

        public Message AddNew(CustomerSuport c)
        {
            Message cusSup;
            HttpResponseMessage response = GlobalVariables.WebApiClient.PostAsJsonAsync("Support", c).Result;
            cusSup = response.Content.ReadAsAsync<Message>().Result;
            return cusSup;
        }
        public CustomerSuport Delete(int id)
        {
            CustomerSuport cusSup;
            HttpResponseMessage response = GlobalVariables.WebApiClient.DeleteAsync("Support/" + id).Result;
            cusSup = response.Content.ReadAsAsync<CustomerSuport>().Result;
            return cusSup;
        }

    }
}