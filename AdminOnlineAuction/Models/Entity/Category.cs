﻿using AdminOnlineAuction.Models.Global;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;

namespace AdminOnlineAuction.Models.Entity
{
    public class Category
    {
        public int ID { get; set; }
        public string CateName { get; set; }
        public string Description { get; set; }
        public string Image { get; set; }
        public int Status { get; set; }
        public DateTime Updated { get; set; }
        public DateTime Created { get; set; }
        public IEnumerable<ProductAuction> ProductAuctions { get; set; }

        public IEnumerable<Category> GetAll()
        {
            IEnumerable<Category> cateList;
            HttpResponseMessage response = GlobalVariables.WebApiClient.GetAsync("Category").Result;
            cateList = response.Content.ReadAsAsync<IEnumerable<Category>>().Result;
            return cateList;

        }

        public Category GetById(int id)
        {
            Category category;
            HttpResponseMessage response = GlobalVariables.WebApiClient.GetAsync("Category/"+id).Result;
            category = response.Content.ReadAsAsync<Category>().Result;
            return category;
        }

        public Category EditCate(Category c)
        {
            Category cate;
            HttpResponseMessage response = GlobalVariables.WebApiClient.PutAsJsonAsync("Category" ,c).Result;
            cate = response.Content.ReadAsAsync<Category>().Result;
            return cate;
        }

        public Category AddCate(Category c)
        {
            Category cate;
            HttpResponseMessage response = GlobalVariables.WebApiClient.PostAsJsonAsync("Category",c).Result;
            cate = response.Content.ReadAsAsync<Category>().Result;
            return cate;
        }
        public Category DeleteCate(int id)
        {
            Category cate;
            HttpResponseMessage response = GlobalVariables.WebApiClient.DeleteAsync("Category/"+id).Result;
            cate = response.Content.ReadAsAsync<Category>().Result;
            return cate;
        }

    }
}