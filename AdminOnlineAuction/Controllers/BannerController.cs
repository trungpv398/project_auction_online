﻿using AdminOnlineAuction.Models.Entity;
using CloudinaryDotNet;
using CloudinaryDotNet.Actions;
using PagedList;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AdminOnlineAuction.Controllers
{
    public class BannerController : Controller
    {
        private Type_Banner type_banner;
        private Banner banner;
        
        public static Cloudinary cloudinary;
        public const string CLOUD_NAME = "dk65dthn5";
        public const string API_KEY = "887159183214426";
        public const string API_SECRET = "4ihijjYSSMO6hBARbJ05lb4k8Ek";
        Account account = new Account(CLOUD_NAME, API_KEY, API_SECRET);
        public BannerController()
        {
            type_banner = new Type_Banner();
            banner = new Banner();
        }
        // GET: Banner
        public ActionResult Index(int? page)
        {

            if (Session["loginAdmin"] == null)
            {
                return RedirectToAction("Login","Home");
            }
            int _page = page ?? 1;
            int _pageSize = 10;
            ViewBag.TypeBanner = type_banner.GetAll().Where(x => x.Status ==1);
            ViewBag.Banner = banner.GetAll().OrderBy(x=>x.ID).ToPagedList(_page,_pageSize);
            return View();
        }

        [HttpPost]
        public ActionResult AddNew(FormCollection form, HttpPostedFileBase file)
        {
            Banner b = new Banner();
            string _File = "";
            string _FileName = "";
            string _path = "";
            cloudinary = new Cloudinary(account);
            try
            {
                if (file.ContentLength > 0)
                {
                    _File = Path.GetFileName(file.FileName);
                    _FileName = _File;
                    _path = Path.Combine(Server.MapPath("~/Assets/uploads"), _FileName);
                    file.SaveAs(_path);
                    var uploadParams = new ImageUploadParams()
                    {
                        File = new FileDescription(_path),

                    };
                    var uploadResult = cloudinary.Upload(uploadParams);
                    b.Image = "https://res.cloudinary.com/dk65dthn5/" + uploadResult.FullyQualifiedPublicId;
                    System.IO.File.Delete(_path);
                }
                b.Created = DateTime.Now;
                b.Title = form["title"];
                b.Description = form["description"];
                b.Status = int.Parse(form["status"]);
                b.TypeBannerID = int.Parse(form["typeBanner"]);
                var data = banner.AddNew(b);
                if (data.StatusCode == 200)
                {
                    TempData["Success"] = "Successfully added..!";
                    return RedirectToAction("Index");
                }
                else
                {
                    TempData["errors"] = "New addition failed..!";
                    return RedirectToAction("Index");
                }
            }
            catch { }
            TempData["errors"] = "New addition failed..!";
            return RedirectToAction("Index");
        }
        public ActionResult EditBanner(int id)
        {
            ViewBag.Data = banner.GetById(id);
            ViewBag.TypeBanner = type_banner.GetAll().Where(x => x.Status == 1);
            return PartialView("_EditBanner");
        }
        [HttpPost]
        public ActionResult UpdateBanner(FormCollection form, HttpPostedFileBase file)
        {
            Banner bUpdate = banner.GetById(int.Parse(form["id"]));
            string _File = "";
            string _FileName = "";
            string _path = "";
            cloudinary = new Cloudinary(account);
            try
            {
                if (file != null)
                {
                    _File = Path.GetFileName(file.FileName);
                    _FileName = _File;
                    _path = Path.Combine(Server.MapPath("~/Assets/uploads"), _FileName);
                    file.SaveAs(_path);
                    var uploadParams = new ImageUploadParams()
                    {
                        File = new FileDescription(_path),

                    };
                    var uploadResult = cloudinary.Upload(uploadParams);
                    bUpdate.Image = "https://res.cloudinary.com/dk65dthn5/" + uploadResult.FullyQualifiedPublicId;
                    System.IO.File.Delete(_path);
                }
              
                bUpdate.Title = form["title"];
                bUpdate.Description = form["description"];
                bUpdate.Status = int.Parse(form["status"]);
                bUpdate.TypeBannerID = int.Parse(form["typeBanner"]);
                var data = banner.Edit(bUpdate);
               
                    TempData["Success"] = "Update successful..!";
                    return RedirectToAction("Index");
               
            }
            catch { }
            TempData["errors"] = "Update failed..!";
            return RedirectToAction("Index");
        }
        public ActionResult Details(int id)
        {
            var data = banner.GetById(id);
            ViewBag.TypeBanner = type_banner.GetAll().Where(x => x.Status == 1);
            ViewBag.DetailBanner = data;
            return PartialView("_Details");
        }
    }
}