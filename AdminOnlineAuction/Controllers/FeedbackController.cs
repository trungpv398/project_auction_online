﻿using AdminOnlineAuction.Models.Entity;
using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AdminOnlineAuction.Controllers
{
    public class FeedbackController : Controller
    {
        private Feedback feedback;
        public FeedbackController()
        {
            feedback = new Feedback();
        }
        // GET: Feedback
        public ActionResult Index(int? page)
        {
            if (Session["loginAdmin"] == null)
            {
                return RedirectToAction("Login", "Home");
            }
            int _page = page ?? 1;
            int _pageSize = 25;
            var data = feedback.GetAll();
            ViewBag.FeedbackData = data.OrderByDescending(x => x.Created).ToPagedList(_page, _pageSize);
            ViewBag.FbCount = data.Where(x=>x.Status==0).Count();
            return View();
        }
        public ActionResult Detail(int id)
        {
           
            var data = feedback.GetById(id);
            if (data.Status == 0)
            {
                data.Status = 1;
                feedback.Edit(data);
            }

            ViewBag.Data = data;
            var dataAllFb = feedback.GetAll();
            ViewBag.FbCount = dataAllFb.Where(x => x.Status == 0).Count();
            return View();
        }
    }
}