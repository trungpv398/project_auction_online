﻿using AdminOnlineAuction.Models.Entity;
using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AdminOnlineAuction.Controllers
{
    public class UserManualsController : Controller
    {
        private UserManuals userManuals;
        public UserManualsController()
        {
            userManuals = new UserManuals();
        }
        // GET: UserManuals
        public ActionResult Index(int? page)
        {
            if (Session["loginAdmin"] == null)
            {
                return RedirectToAction("Login", "Home");
            }
            int _page = page ?? 1;
            int _pageSize = 10;
            ViewBag.userManuals = userManuals.GetAll().OrderBy(x=>x.ID).ToPagedList(_page,_pageSize);
            return View();
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult AddManuals(UserManuals u)
        {
            u.Status = 1;
            u.Created = DateTime.Now;
            if(ModelState.IsValid)
            {
                var data = userManuals.AddNew(u);
                if (data.StatusCode == 200)
                {
                    TempData["Success"] = "Successfully added..!";
                    return RedirectToAction("Index");

                }
                else
                {
                    TempData["errors"] = "New addition failed....!";
                    return RedirectToAction("Index");

                }
            }
            return RedirectToAction("Index");

        }

        public ActionResult Edit(int id)
        {
            UserManuals data = userManuals.GetById(id);
            return View(data);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Update(UserManuals u,int status)
        {
            UserManuals uUpdate = userManuals.GetById(u.ID);
            uUpdate.Title = u.Title;
            uUpdate.Content = u.Content;
            uUpdate.Status = status;
            try
            {
                userManuals.Edit(uUpdate);
                TempData["Success"] = "Update successful..!";
                return RedirectToAction("Index");
            }
            catch { }
            TempData["errors"] = "Update failed..!";
            return View();


        }

        public ActionResult Detail(int id)
        {
            var data = userManuals.GetById(id);
            ViewBag.Data = data;

            return View();
        }
    }
}