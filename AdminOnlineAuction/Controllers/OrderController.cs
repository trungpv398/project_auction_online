﻿using AdminOnlineAuction.Common;
using AdminOnlineAuction.Models.Entity;
using PagedList;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AdminOnlineAuction.Controllers
{
    public class OrderController : Controller
    {
        private Order order;
        private ProductAuction productAuction;
        private SalesMan salesMan;
        public OrderController()
        {
            order           = new Order();
            productAuction  = new ProductAuction();
            salesMan        = new SalesMan();
        }
        // GET: All Order
        public ActionResult Index(int? page, int? key)
        {
            if (Session["loginAdmin"] == null)
            {
                return RedirectToAction("Login", "Home");
            }
            order.CheckOrder(0,4);
            int _page = page ?? 1;
            int _pageSize = 10;
            int _key = key ?? 0;
            var data = order.GetOrderAll().OrderBy(x => x.status);
            if(_key!=0)
            {
                ViewBag.OrderAll = data.Where(x=>x.ID == key).ToPagedList(_page, _pageSize);
            }
            else
            {
                ViewBag.OrderAll = data.ToPagedList(_page, _pageSize);
            }
            GetCountStatus();
            ViewBag.StatusAll= data.Count();
            return View();
        }

        public ActionResult GetStatus(int status,int? page,int? key)
        {
            int _page = page ?? 1;
            int _pageSize = 10;
            int _key = key ?? 0;
            ViewBag.Status = status;
            var data = order.GetOdByStatus(status).OrderBy(x => x.Created);
            if(_key!=0)
            {
                ViewBag.OrderStatus = data.Where(x=>x.ID==key).ToPagedList(_page, _pageSize);
            }
            else
            {
                ViewBag.OrderStatus = data.ToPagedList(_page, _pageSize);
            }
            GetCountStatus();
            return View();
        }

        [HttpPost]
        public ActionResult UpdateStatus(FormCollection form)
        {
            var data = order.OrderDetail(int.Parse(form["id"]));
            data.status = int.Parse(form["status"]);
            order.EditOrder(data);
            productAuction.UpdateStSuccessPro(3);
            var dataSaleMan = salesMan.GetById(data.Auction.ProductAuction.SaleManID);
            //mail
            string content = System.IO.File.ReadAllText(Server.MapPath("~/Assets/Template/neworder.html"));
            string content2 = System.IO.File.ReadAllText(Server.MapPath("~/Assets/Template/neworder2.html"));

            string contentmail = "";
            if(int.Parse(form["status"])==2)
            {
                contentmail = "Your order is being shipped.";
            }else if(int.Parse(form["status"]) == 3)
            {
                contentmail = "Your order is completed.";
            }else if(int.Parse(form["status"]) == 4)
            {
                contentmail = "Your order has been canceled";
            }
            else if (int.Parse(form["status"]) == 1)
            {
                contentmail = "your order is waiting to be picked up";
            }
            //content 1
            content = content.Replace("{{Image}}", data.Auction.ProductAuction.Image);
            content = content.Replace("{{ProName}}", data.Auction.ProductAuction.ProName);
            content = content.Replace("{{ProCode}}", Convert.ToString(data.Auction.ProductAuction.ID));
            content = content.Replace("{{Email}}", dataSaleMan.Email);
            content = content.Replace("{{ContentVl}}", contentmail);
            content = content.Replace("{{Phone}}", data.Phone);
            content = content.Replace("{{Note}}", Convert.ToString(data.ID));
            content = content.Replace("{{Fullname}}", data.Receiver);
            content = content.Replace("{{pathToFile}}", "https://res.cloudinary.com/dk65dthn5/image/upload/v1611217961/logo2_lidt9c.png");
            content = content.Replace("{{Total}}", data.TotalPrice.ToString("N0"));
            content = content.Replace("{{TotalFees}}", data.AuctionFees.ToString("N0"));
            content = content.Replace("{{TotalReceived}}", data.MoneyReceived.ToString("N0"));
            
            var toEmail = ConfigurationManager.AppSettings["ToEmailAddress"].ToString();

            new MailHelper().SendMail(dataSaleMan.Email, "Đơn hàng mới từ OnlineShop", content);
            new MailHelper().SendMail(toEmail, "Đơn hàng mới từ OnlineShop", content);
            //content 2
            content2 = content2.Replace("{{Image}}", data.Auction.ProductAuction.Image);
            content2 = content2.Replace("{{ProName}}", data.Auction.ProductAuction.ProName);
            content2 = content2.Replace("{{ProCode}}", Convert.ToString(data.Auction.ProductAuction.ID));
            content2 = content2.Replace("{{Email}}", data.Email);
            content2 = content2.Replace("{{ContentVl}}", contentmail);
            content2 = content2.Replace("{{Phone}}", data.Phone);
            content2 = content2.Replace("{{Note}}", Convert.ToString(data.ID));
            content2 = content2.Replace("{{Fullname}}", data.Receiver);
            content2 = content2.Replace("{{pathToFile}}", "https://res.cloudinary.com/dk65dthn5/image/upload/v1611217961/logo2_lidt9c.png");
            content2 = content2.Replace("{{Total}}", data.TotalPrice.ToString("N0"));
            var toEmail2 = ConfigurationManager.AppSettings["ToEmailAddress"].ToString();

            new MailHelper().SendMail2(data.Email, "Đơn hàng mới từ OnlineShop", content2);
            new MailHelper().SendMail2(toEmail2, "Đơn hàng mới từ OnlineShop", content2);


            TempData["Success"] = "Successfully update..!";
            return RedirectToAction("DetailOrder",new { id = int.Parse(form["id"])});
        }

        public ActionResult Detail(int id)
        {
            var data = order.OrderDetail(id);
            ViewBag.Data = data;
            ViewBag.SaleMan = salesMan.GetById(data.Auction.ProductAuction.SaleManID);
            ViewBag.Pro = order.GetAutionByProId(data.AutionID);
            return PartialView("_Detail");
        }
        public ActionResult DetailOrder(int id)
        {
            var data = order.OrderDetail(id);
            ViewBag.Data = data;
            ViewBag.SaleMan = salesMan.GetById(data.Auction.ProductAuction.SaleManID);
            ViewBag.Pro = order.GetAutionByProId(data.AutionID);
            return View();
        }

        public void GetCountStatus()
        {
            ViewBag.Status0 = order.CountOrder(0);
            ViewBag.Status1 = order.CountOrder(1);
            ViewBag.Status2 = order.CountOrder(2);
            ViewBag.Status3 = order.CountOrder(3);
            ViewBag.Status4 = order.CountOrder(4);
        }
    }
}