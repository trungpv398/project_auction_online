﻿using AdminOnlineAuction.Common;
using AdminOnlineAuction.Models.Entity;
using PagedList;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AdminOnlineAuction.Controllers
{
    public class ProductAuctionController : Controller
    {

        private ProductAuction productAuction;
        private Auction auctions;
        private Order order;
        public ProductAuctionController()
        {
            productAuction = new ProductAuction();
            auctions = new Auction();
            order = new Order();
        }

        // GET: ProductAuction
        public ActionResult Index(int? page, int? key,int? date)
        {
            if (Session["loginAdmin"] == null)
            {
                return RedirectToAction("Login", "Home");
            }
            productAuction.UpdateStSuccessPro(3);
            int _page = page ?? 1;
           
            int _pageSize = 10;
            int _key = key ?? 0;
            int _date = date ?? 0;
            var dataPro = productAuction.GetProAll().OrderByDescending(x => x.EndDateTime);
            

            if(_key != 0)
            {
                if(_date != 0)
                {ViewBag.ProductAuction1 = dataPro.Where(x => x.ID == key && x.EndDateTime.Month==date).ToPagedList(_page, _pageSize);}
                else
                {ViewBag.ProductAuction1 = dataPro.Where(x => x.ID == key).ToPagedList(_page, _pageSize);}
            }
            else
            {
                if(_date != 0)
                {ViewBag.ProductAuction1 = dataPro.Where(x=> x.EndDateTime.Month == date).ToPagedList(_page, _pageSize);}
                else
                {ViewBag.ProductAuction1 = dataPro.ToPagedList(_page, _pageSize);}
            }


            ViewBag.StatusAll = dataPro.Count();
            ViewBag.Status0 = productAuction.CoutPro(0);
            ViewBag.Status1 = productAuction.CoutPro(1);
            ViewBag.Status2 = productAuction.CoutPro(2)+ productAuction.CoutPro(3)+ productAuction.CoutPro(4)+ productAuction.CoutPro(5);
            /*ViewBag.Status3 = productAuction.CoutPro(3);
            ViewBag.Status4 = productAuction.CoutPro(4);*/
            ViewBag.Date = _date;
            ViewBag.Key = _key;
            return View();
        }
        public ActionResult GetAction2(int status,int? page,int? key, int? date)
        {
            int _page = page ?? 1;
            int _pageSize = 10;
            int _key = key ?? 0;
            var dataPro = productAuction.GetProAll();
            int _date = date ?? 0;
            if (_key!=0)
            {
                if(status==2)
                {
                    if(_date!=0)
                    {
                        ViewBag.ProductAuction1 = dataPro.Where(x => x.Status == status || x.Status == 3 || x.Status == 4 || x.Status == 5 && x.ID == key&&x.EndDateTime.Month==_date).OrderByDescending(x => x.EndDateTime).ToPagedList(_page, _pageSize);

                    }
                    else
                    {
                        ViewBag.ProductAuction1 = dataPro.Where(x => x.Status == status || x.Status == 3 || x.Status == 4 || x.Status == 5 && x.ID == key).OrderByDescending(x => x.EndDateTime).ToPagedList(_page, _pageSize);

                    }
                }
                else
                {
                    if(_date!=0)
                    {
                        ViewBag.ProductAuction1 = dataPro.Where(x => x.Status == status && x.ID == key && x.EndDateTime.Month==_date).OrderByDescending(x => x.EndDateTime).ToPagedList(_page, _pageSize);

                    }
                    else
                    {
                        ViewBag.ProductAuction1 = dataPro.Where(x => x.Status == status && x.ID == key).OrderByDescending(x => x.EndDateTime).ToPagedList(_page, _pageSize);

                    }
                }
            }
            else
            {
                if(status==2)
                {
                    if(_date!=0)
                    {
                        ViewBag.ProductAuction1 = dataPro.Where(x => x.Status == status || x.Status == 3 || x.Status == 4 || x.Status == 5&&x.EndDateTime.Month==_date).OrderByDescending(x => x.EndDateTime).ToPagedList(_page, _pageSize);

                    }
                    else
                    {
                        ViewBag.ProductAuction1 = dataPro.Where(x => x.Status == status || x.Status == 3 || x.Status == 4 || x.Status == 5).OrderByDescending(x => x.EndDateTime).ToPagedList(_page, _pageSize);

                    }
                }
                else
                {
                    if(_date!=0)
                    {
                        ViewBag.ProductAuction1 = dataPro.Where(x => x.Status == status&&x.EndDateTime.Month==_date).OrderByDescending(x => x.EndDateTime).ToPagedList(_page, _pageSize);

                    }
                    else
                    {
                        ViewBag.ProductAuction1 = dataPro.Where(x => x.Status == status).OrderByDescending(x => x.EndDateTime).ToPagedList(_page, _pageSize);

                    }
                }
            }

            ViewBag.StatusPresent = status;
            ViewBag.Status0 = productAuction.CoutPro(0);
            ViewBag.Status1 = productAuction.CoutPro(1);
            ViewBag.Status2 = productAuction.CoutPro(2) + productAuction.CoutPro(3) + productAuction.CoutPro(4) + productAuction.CoutPro(5);
            ViewBag.Date = _date;
            ViewBag.Key = _key;
            ViewBag.StatusAll = dataPro.Count();
            return View();
        }
        public ActionResult Detail(int id)
        {
            ViewBag.Detail = productAuction.GetProByID(id);
            return PartialView("_Detail");
        }

       
        //check ai la nguoi chien thang roi tao don
        public ActionResult CreateOrder(int proid)
        {
            ViewBag.ProAuction = productAuction.ProductByID(proid);
            ViewBag.Auction = auctions.GetListByPro(proid).OrderByDescending(x=>x.Created);
            return View();
        }
        //check don hang
        public ActionResult CheckOrder(int id)
        {
         
            var data = order.OrderByAutionID(id);
            ViewBag.Data = data;
            ViewBag.Pro = order.GetAutionByProId(data.AutionID);
            return PartialView("_CheckOrder");
        }

        public ActionResult CheckAuStatus(int auid,int status)
        {
            var auData = auctions.GetById(auid);
            var data = auctions.GetListByPro(auData.ProAutionID);
            foreach(var item in data)
            {
                if(item.ID != auData.ID)
                {
                    item.Status = 2;
                    auctions.Edit(item);
                }
            }

            auData.Status = status;
            auctions.Edit(auData);
            

            TempData["Success"] = "Successful confirmation..!";
            return RedirectToAction("CreateOrder", new { proid = auData.ProAutionID });
        }
       
        public ActionResult CreateOrders(int auid)
        {
            var date_time_now = DateTime.Now;
            var data = auctions.GetAcByID(auid);
            data.Status = 3;
            auctions.Edit(data);
            Order oderNew = new Order();
            oderNew.CustomerID = data.CustomerID;
            oderNew.ProAutionID = data.ProAutionID;
            oderNew.Created = date_time_now;
            oderNew.EndDateTime = date_time_now.AddDays(1);
            oderNew.AutionID = data.ID;
            oderNew.status = 0;
            oderNew.TotalPrice = data.Price;
            var aucfee = (data.Price * 5) / 100;
            var moneyRece = data.Price - aucfee;
            oderNew.AuctionFees = aucfee;
            oderNew.MoneyReceived = moneyRece;
            var orderData = order.AddNew(oderNew);

            string content2 = System.IO.File.ReadAllText(Server.MapPath("~/Assets/Template/neworder2.html"));
            string contentmail = "Congratulations on your successful order,We have received the order, will contact you as soon as possible...";
            if (orderData.StatusCode==200)
            {
                //content 2
               /* content2 = content2.Replace("{{Image}}", data.ProductAuction.Image);
                content2 = content2.Replace("{{ProName}}", data.ProductAuction.ProName);
                content2 = content2.Replace("{{ProCode}}", Convert.ToString(data.ProductAuction.ID));
                content2 = content2.Replace("{{Email}}", oderNew.Email);
                content2 = content2.Replace("{{ContentVl}}", contentmail);
                content2 = content2.Replace("{{Phone}}", oderNew.Phone);
                content2 = content2.Replace("{{Note}}", Convert.ToString(data.ID));
                content2 = content2.Replace("{{Fullname}}", oderNew.Receiver);
                content2 = content2.Replace("{{pathToFile}}", "https://res.cloudinary.com/dk65dthn5/image/upload/v1611217961/logo2_lidt9c.png");
                content2 = content2.Replace("{{Total}}", oderNew.TotalPrice.ToString("N0"));
                var toEmail2 = ConfigurationManager.AppSettings["ToEmailAddress"].ToString();

                new MailHelper().SendMail2(oderNew.Email, "Đơn hàng mới từ OnlineShop", content2);
                new MailHelper().SendMail2(toEmail2, "Đơn hàng mới từ OnlineShop", content2);*/
                TempData["Success"] = "Successfully created an order..!";
                return RedirectToAction("CreateOrder", new { proid = data.ProAutionID });
            }
            else
            {
                TempData["errors"] = "Create a failed order..!";
                return RedirectToAction("CreateOrder", new { proid = data.ProAutionID });
            }
        }
    }
}