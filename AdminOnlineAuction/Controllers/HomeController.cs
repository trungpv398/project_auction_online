﻿using AdminOnlineAuction.Models.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AdminOnlineAuction.Controllers
{
    public class HomeController : Controller
    {
        private User user;
        private Order order;
        private Customer customer;
        private ProductAuction productAuction;
        public HomeController()
        {
            user = new User();
            order = new Order();
            customer = new Customer();
            productAuction = new ProductAuction();
        }
        public ActionResult Index()
        {
            if (Session["loginAdmin"] == null)
            {
                return RedirectToAction("Login");
            }
            order.CheckOrder(0,4);
            RevenueInformation();
            ViewBag.CustomerRate = customer.GetAll().OrderByDescending(x => x.Rate).Take(10);
            ViewBag.ProEndAuction = productAuction.GetAll().Where(x=>x.Status==2).OrderByDescending(x=>x.EndDateTime);
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
        public ActionResult Login()
        {
          
            return View();
        }

        [HttpPost]
        public ActionResult Login(FormCollection form)
        {
            var data = user.GetAll();
            var pwd = user.GETSHA(form["pass"]);
            foreach(var item in data)
            {
                if(item.Email == form["email"])
                {
                    if(item.Password == pwd)
                    {
                        User u = item;
                        Session["loginAdmin"] = u;
                        TempData["Success"] = "Successfully login..!";
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        TempData["errors"] = "Your password is incorrect..!";
                        return RedirectToAction("Login");
                    }
                }
                else
                {
                    TempData["errors"] = "Your email is incorrect..!";
                    return RedirectToAction("Login");
                }
            }
            TempData["errors"] = "Your account is incorrect..!";
            return View();
        }

        public ActionResult Register()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Register(FormCollection form)
        {
            try
            {
                User us = new User();
                us.Created = DateTime.Now;
                us.Email = form["email"];
                us.FullName = form["fullname"];
                us.Status = 0;
                us.Password = form["pass"];
                us.PhoneNumber = form["phonenumber"];
                var data = user.AddNew(us);
                if (data.StatusCode == 200)
                {
                    TempData["Success"] = "Successfully register..!";
                    return RedirectToAction("Login");
                }
                else
                {
                    TempData["errors"] = "Register failed..!";
                    return RedirectToAction("Login");

                }
            }
            catch { }

            TempData["errors"] = "Register failed..!";
            return RedirectToAction("Register");
        }

        public ActionResult Logout()
        {
            Session["loginAdmin"] = null;
            TempData["Success"] = "Successfully logout..!";
            return RedirectToAction("Index");
        }
        public  void RevenueInformation()
        {
            ViewBag.Statistical1 = order.TotalAuctionFees(3, 1);
            ViewBag.Statistical2 = order.TotalAuctionFees(3, 2);
            ViewBag.Statistical3 = order.TotalAuctionFees(3, 3);
            ViewBag.Statistical4 = order.TotalAuctionFees(3, 4);
            ViewBag.Statistical5 = order.TotalAuctionFees(3, 5);
            ViewBag.Statistical6 = order.TotalAuctionFees(3, 6);
            ViewBag.Statistical7 = order.TotalAuctionFees(3, 7);
            ViewBag.Statistical8 = order.TotalAuctionFees(3, 8);
            ViewBag.Statistical9 = order.TotalAuctionFees(3, 9);
            ViewBag.Statistical10 = order.TotalAuctionFees(3, 10);
            ViewBag.Statistical11 = order.TotalAuctionFees(3, 11);
            ViewBag.Statistical12 = order.TotalAuctionFees(3, 12);

            ViewBag.StatisticalTTP1 = order.TotalTotalPrice(3, 1);
            ViewBag.StatisticalTTP2 = order.TotalTotalPrice(3, 2);
            ViewBag.StatisticalTTP3 = order.TotalTotalPrice(3, 3);
            ViewBag.StatisticalTTP4 = order.TotalTotalPrice(3, 4);
            ViewBag.StatisticalTTP5 = order.TotalTotalPrice(3, 5);
            ViewBag.StatisticalTTP6 = order.TotalTotalPrice(3, 6);
            ViewBag.StatisticalTTP7 = order.TotalTotalPrice(3, 7);
            ViewBag.StatisticalTTP8 = order.TotalTotalPrice(3, 8);
            ViewBag.StatisticalTTP9 = order.TotalTotalPrice(3, 9);
            ViewBag.StatisticalTTP10 = order.TotalTotalPrice(3, 10);
            ViewBag.StatisticalTTP11 = order.TotalTotalPrice(3, 11);
            ViewBag.StatisticalTTP12 = order.TotalTotalPrice(3, 12);

            ViewBag.StatisticalTMR1 = order.TotalMoneyReceived(3, 1);
            ViewBag.StatisticalTMR2 = order.TotalMoneyReceived(3, 2);
            ViewBag.StatisticalTMR3 = order.TotalMoneyReceived(3, 3);
            ViewBag.StatisticalTMR4 = order.TotalMoneyReceived(3, 4);
            ViewBag.StatisticalTMR5 = order.TotalMoneyReceived(3, 5);
            ViewBag.StatisticalTMR6 = order.TotalMoneyReceived(3, 6);
            ViewBag.StatisticalTMR7 = order.TotalMoneyReceived(3, 7);
            ViewBag.StatisticalTMR8 = order.TotalMoneyReceived(3, 8);
            ViewBag.StatisticalTMR9 = order.TotalMoneyReceived(3, 9);
            ViewBag.StatisticalTMR10 = order.TotalMoneyReceived(3, 10);
            ViewBag.StatisticalTMR11 = order.TotalMoneyReceived(3, 11);
            ViewBag.StatisticalTMR12 = order.TotalMoneyReceived(3, 12);
        }

    }
}