﻿using AdminOnlineAuction.Models.Entity;
using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AdminOnlineAuction.Controllers
{
    public class TypeProductController : Controller
    {
        private Type_Product type_Product;
        public TypeProductController()
        {
            type_Product = new Type_Product();
        }
        // GET: TypeProduct
        public ActionResult Index(int? page)
        {
            if (Session["loginAdmin"] == null)
            {
                return RedirectToAction("Login", "Home");
            }
            int _page = page ?? 1;
            int _pageSize = 10;
            ViewBag.data = type_Product.GetAll().OrderBy(x=>x.ID).ToPagedList(_page,_pageSize);
            return View();
        }

        [HttpPost]
        public ActionResult AddType(FormCollection form)
        {
            Type_Product tp = new Type_Product();
            tp.TypeProName = form["typeName"];
            tp.Created = DateTime.Now;
            tp.Status = 1;
            try
            {
                var data = type_Product.AddType(tp);
                if (data.StatusCode == 200)
                {
                    TempData["Success"] = "Successfully added..!";
                    return RedirectToAction("Index");
                }
                else
                {
                    TempData["Success"] = "New addition failed..!";
                    return RedirectToAction("Index");
                }              
            }
            catch { }
            return RedirectToAction("Index");
        }

        /*[HttpPost]
        public ActionResult AddType(Type_Product t)
        {
            Type_Product tp = new Type_Product();
            tp.TypeProName = t.TypeProName;
            tp.Created = DateTime.Now;
            tp.Status = 1;
            type_Product.AddType(tp);
            return RedirectToAction("Index");
        }*/

        public ActionResult EditType(int id)
        {
            var data = type_Product.GetById(id);
            ViewBag.TypePro = data;
            return PartialView("_EditType");
        }
        [HttpPost]
        public ActionResult UpdateType(FormCollection form)
        {
            Type_Product tp = type_Product.GetById(int.Parse(form["id"]));
            tp.TypeProName = form["typeName"];
            tp.Status = int.Parse(form["status"]);
            type_Product.EditType(tp);
            TempData["Success"] = "Update successful..!";
            return RedirectToAction("Index");
        }

        public ActionResult Details(int id)
        {
            var data = type_Product.GetById(id);
            ViewBag.TypePro = data;
            return PartialView("_Details");
        }
    }
}