﻿using AdminOnlineAuction.Models.Entity;
using CloudinaryDotNet;
using CloudinaryDotNet.Actions;
using PagedList;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AdminOnlineAuction.Controllers
{
    public class CategoryController : Controller
    {
        public static Cloudinary cloudinary;
        public const string CLOUD_NAME = "dk65dthn5";
        public const string API_KEY = "887159183214426";
        public const string API_SECRET = "4ihijjYSSMO6hBARbJ05lb4k8Ek";
        Account account = new Account(CLOUD_NAME, API_KEY, API_SECRET);

        private Category category;
        public CategoryController()
        {
            category = new Category();
        }
        // GET: Category
        public ActionResult Index(int? page)
        {
            if (Session["loginAdmin"] == null)
            {
                return RedirectToAction("Login", "Home");
            }
            int _page = page ?? 1;
            int _pageSize = 10;
            var data = category.GetAll();
            return View(data.OrderBy(x=>x.ID).ToPagedList(_page,_pageSize));
        }
        [HttpPost]
        public ActionResult AddCate(FormCollection form, HttpPostedFileBase file)
        {
            Category c = new Category();
            string _File = "";
            string _FileName = "";
            string _path = "";
            cloudinary = new Cloudinary(account);
           
            try
            {

                if (file.ContentLength > 0)
                {
                    _File = Path.GetFileName(file.FileName);
                    _FileName = _File;
                    _path = Path.Combine(Server.MapPath("~/Assets/uploads"), _FileName);
                    file.SaveAs(_path);
                    var uploadParams = new ImageUploadParams()
                    {
                        File = new FileDescription(_path),

                    };
                    var uploadResult = cloudinary.Upload(uploadParams);
                    c.Image = "https://res.cloudinary.com/dk65dthn5/" + uploadResult.FullyQualifiedPublicId;
                    System.IO.File.Delete(_path);

                }
                c.CateName = form["cateName"];
                c.Description = form["desription"];
                c.Created = DateTime.Now;
                c.Updated = DateTime.Now;
                c.Status = 1;
                category.AddCate(c);
                TempData["Success"] = "Successfully added..!";
                return RedirectToAction("Index");
            }
            catch { return View(); }
              
        }

        public ActionResult EditCate(int id)
        {
            var data = category.GetById(id);
            ViewBag.EditCate = data;
            return PartialView("_EditCate");
        }

        [HttpPost]
        public ActionResult UpdateCate(FormCollection form, HttpPostedFileBase file)
        {
            Category c = category.GetById(int.Parse(form["id"]));
            string _File = "";
            string _FileName = "";
            string _path = "";
            cloudinary = new Cloudinary(account);
            try
            {
                if (file != null)
                {
                    _File = Path.GetFileName(file.FileName);
                    _FileName = _File;
                    _path = Path.Combine(Server.MapPath("~/Assets/uploads"), _FileName);
                    file.SaveAs(_path);
                    var uploadParams = new ImageUploadParams()
                    {
                        File = new FileDescription(_path),
                    };
                    var uploadResult = cloudinary.Upload(uploadParams);
                    c.Image = "https://res.cloudinary.com/dk65dthn5/" + uploadResult.FullyQualifiedPublicId;
                    System.IO.File.Delete(_path);
                }
              
               
                c.CateName = form["cateName"];
                c.Description = form["desription"];
                c.Updated = DateTime.Now;
                c.Status = int.Parse(form["status"]);
                category.EditCate(c);
                TempData["Success"] = "Update successful..!";
                return RedirectToAction("Index");
            }
            catch { return View(); }
        }

        public ActionResult Details(int id)
        {
            var data = category.GetById(id);
            ViewBag.EditCate = data;
            return PartialView("_Details");
        }


       
    }
}