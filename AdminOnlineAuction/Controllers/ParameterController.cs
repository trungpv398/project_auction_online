﻿using AdminOnlineAuction.Models.Entity;
using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AdminOnlineAuction.Controllers
{
    public class ParameterController : Controller
    {
        private Type_Product type_Product;
        private Parameter parameter;
        public ParameterController()
        {
            type_Product = new Type_Product();
            parameter = new Parameter();
        }
        // GET: Parameter
        public ActionResult Index(int? page,string key)
        {
            if (Session["loginAdmin"] == null)
            {
                return RedirectToAction("Login", "Home");
            }
            int _page = page ?? 1;
            int _pageSize = 10;
            ViewBag.Type = type_Product.GetAll().Where(x => x.Status == 1);
            if (key != null)
            {
                ViewBag.Parameter = parameter.GetAll().Where(x=>x.ParameterName.Contains(key)).OrderBy(x => x.ID).ToPagedList(_page, _pageSize);

            }
            else
            {
                ViewBag.Parameter = parameter.GetAll().OrderBy(x => x.ID).ToPagedList(_page, _pageSize);

            }

            ViewBag.TypeProID = new SelectList(type_Product.GetAll(), "ID", "TypeProName");
            return View();
        }

        [HttpPost]
        public ActionResult AddNew(FormCollection form)
        {
            Parameter p = new Parameter();
            p.ParameterName = form["nameParam"];
            p.Status = 1;
            p.Created = DateTime.Now;
            p.TypeProID = int.Parse(form["typePro"]);
            var data = parameter.AddNew(p);
            if(data.StatusCode ==200)
            {
                TempData["Success"] = "Successfully added..!";
                return RedirectToAction("Index");
            }else
            {
                TempData["Success"] = "New addition failed..!";
                return RedirectToAction("Index");
            }       
        }

        public ActionResult Edit(int id)
        {
            var data = parameter.GetById(id);
            ViewBag.Param = data;
            ViewBag.Type = type_Product.GetAll().Where(x => x.Status == 1);
            return PartialView("_Edit");
        }

        [HttpPost] 
        public ActionResult Update(FormCollection form)
        {
            Parameter p = parameter.GetById(int.Parse(form["id"]));
            p.ParameterName = form["nameParam"];
            p.Status = int.Parse(form["status"]);
            p.TypeProID = int.Parse(form["typePro"]);
            parameter.Edit(p);
            TempData["Success"] = "Update successful...!";
            return RedirectToAction("Index");
        }
        public ActionResult Details(int id)
        {
            var data = parameter.GetById(id);
            ViewBag.Param = data;
            ViewBag.Type = type_Product.GetAll().Where(x => x.Status == 1);
            return PartialView("_Details");
        }
    }
}