﻿using AdminOnlineAuction.Models.Entity;
using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AdminOnlineAuction.Controllers
{
    public class TypeBannerController : Controller
    {
        private Type_Banner type_banner;
        public TypeBannerController()
        {
            type_banner = new Type_Banner();
        }
        // GET: TypeBanner
        public ActionResult Index(int? page)
        {
            if (Session["loginAdmin"] == null)
            {
                return RedirectToAction("Login", "Home");
            }
            int _page = page ?? 1;
            int _pageSize = 10;
            ViewBag.Banner = type_banner.GetAll().OrderBy(x=>x.ID).ToPagedList(_page,_pageSize);
            return View();
        }
        [HttpPost]
        public ActionResult AddNew(FormCollection form)
        {
            Type_Banner tb = new Type_Banner();
            tb.Status = 1;
            tb.Created = DateTime.Now;
            tb.Description = form["description"];
            tb.Title = form["title"];
            
            var data = type_banner.AddNew(tb);
            if(data.StatusCode ==200)
            {
                TempData["Success"] = "Successfully added..!";
                return RedirectToAction("Index");

            }
            TempData["Success"] = "New addition failed..!";
            return RedirectToAction("Index");
        }
        public ActionResult EditTypeBanner(int id)
        {
            ViewBag.Data = type_banner.GetById(id);
            return PartialView("_EditTypeBanner");
        }
        [HttpPost]
        public ActionResult UpdateTypeBanner(FormCollection form)
        {
            Type_Banner tb = type_banner.GetById(int.Parse(form["id"]));
            try
            {
                tb.Description = form["description"];
                tb.Status = int.Parse(form["status"]);
                tb.Title = form["title"];
                 type_banner.Edit(tb);
               
                    TempData["Success"] = "Update successful..!";
                    return RedirectToAction("Index");
                
            }
            catch { }
            TempData["errors"] = "Update failed..!";
            return RedirectToAction("Index");
        }
        public ActionResult Details(int id)
        {
            var data = type_banner.GetById(id);
            ViewBag.TypeBanner = data;
            return PartialView("_Details");
        }
    }
}