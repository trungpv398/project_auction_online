﻿using AdminOnlineAuction.Models.Entity;
using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AdminOnlineAuction.Controllers
{
    public class SupportController : Controller
    {
        private CustomerSuport cust_suport;
        public SupportController()
        {
            cust_suport = new CustomerSuport();
        }
        // GET: Support
        public ActionResult Index(int? page)
        {
            if (Session["loginAdmin"] == null)
            {
                return RedirectToAction("Login", "Home");
            }
            int _page = page ?? 1;
            int _pageSize = 10;
            var data = cust_suport.GetAll().OrderBy(x=>x.ID).ToPagedList(_page,_pageSize);
            return View(data);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult AddData(FormCollection form)
        {
            CustomerSuport c = new CustomerSuport();
            c.Title = form["title"];
            c.Content = form["content"];
            c.Status = 1;
            c.Created = DateTime.Now;
            if (ModelState.IsValid)
            {
                var data = cust_suport.AddNew(c);
                if (data.StatusCode==200)
                {
                    TempData["Success"] = "Successfully added..!!";
                }
                else
                {
                    TempData["errors"] = "New addition failed....!";
                }
                return RedirectToAction("Index");
            }
           
                TempData["errors"] = "New addition failed....!";
                return RedirectToAction("Index");
        }

        public ActionResult EditSupport(int id)
        {
            CustomerSuport data = cust_suport.GetById(id);

            return View(data);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult UpdateSuport(CustomerSuport c,int status)
        {
            CustomerSuport cUpdate = cust_suport.GetById(c.ID);
            cUpdate.Title = c.Title;
            cUpdate.Content = c.Content;
            cUpdate.Status = status;
            try
            {
                cust_suport.Edit(cUpdate);
                TempData["Success"] = "Update successful..!";
                return RedirectToAction("Index");
            }
            catch { }

            TempData["errors"] = "Update failed..!";
            return View();
        }
        public ActionResult Details(int id)
        {
            var data = cust_suport.GetById(id);
            ViewBag.Data = data;
            return View();
        }
    }
}