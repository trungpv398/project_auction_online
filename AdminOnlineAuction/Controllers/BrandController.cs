﻿using AdminOnlineAuction.Models.Entity;
using CloudinaryDotNet;
using CloudinaryDotNet.Actions;
using PagedList;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AdminOnlineAuction.Controllers
{
    public class BrandController : Controller
    {
        private Brand brand;
        private Type_Product type_Product;
        public static Cloudinary cloudinary;
        public const string CLOUD_NAME = "dk65dthn5";
        public const string API_KEY = "887159183214426";
        public const string API_SECRET = "4ihijjYSSMO6hBARbJ05lb4k8Ek";
        Account account = new Account(CLOUD_NAME, API_KEY, API_SECRET);
        public BrandController()
        {
            brand = new Brand();
            type_Product = new Type_Product();
        }
        // GET: Brand
        public ActionResult Index(int?  page)
        {
            if (Session["loginAdmin"] == null)
            {
                return RedirectToAction("Login", "Home");
            }
            int _page = page ?? 1;
            int _pageSize = 10;
            ViewBag.Data = brand.GetAll().OrderBy(x=>x.ID).ToPagedList(_page,_pageSize);
            ViewBag.Type = type_Product.GetAll().Where(x => x.Status == 1);
            return View();
        }

        [HttpPost]
        public ActionResult AddBrand(FormCollection form, HttpPostedFileBase file)
        {
            Brand b = new Brand();
            string _File = "";
            string _FileName = "";
            string _path = "";
            cloudinary = new Cloudinary(account);
            
                if (file != null)
                {
                    _File = Path.GetFileName(file.FileName);
                    _FileName = _File;
                    _path = Path.Combine(Server.MapPath("~/Assets/uploads"), _FileName);
                    file.SaveAs(_path);
                    var uploadParams = new ImageUploadParams()
                    {
                        File = new FileDescription(_path),

                    };
                    var uploadResult = cloudinary.Upload(uploadParams);
                    b.Image = "https://res.cloudinary.com/dk65dthn5/" + uploadResult.FullyQualifiedPublicId;
                    System.IO.File.Delete(_path);

                }
                b.Created = DateTime.Now;
                b.BrandName = form["nameBrand"];
                b.Status = 1;
                b.TypeProID = int.Parse(form["typePro"]);
                var data = brand.AddNew(b);
                if (data.StatusCode == 200)
                {
                    TempData["Success"] = "Successfully added..!";
                    return RedirectToAction("Index");
                }
                else
                {
                    TempData["Success"] = "New addition failed..!";
                    return RedirectToAction("Index");
                }             
        }

        public ActionResult EditBrand(int id)
        {
            ViewBag.Data = brand.GetById(id);
            ViewBag.Type = type_Product.GetAll().Where(x => x.Status == 1);
            return PartialView("EditBrand");
        }

        [HttpPost]
        public ActionResult UpdateBrand(FormCollection form, HttpPostedFileBase file)
        {
            Brand b = brand.GetById(int.Parse(form["id"]));
            string _File = "";
            string _FileName = "";
            string _path = "";
            cloudinary = new Cloudinary(account);
            try { 
            if (file != null)
            {
                _File = Path.GetFileName(file.FileName);
                _FileName = _File;
                _path = Path.Combine(Server.MapPath("~/Assets/uploads"), _FileName);
                file.SaveAs(_path);
                var uploadParams = new ImageUploadParams()
                {
                    File = new FileDescription(_path),

                };
                var uploadResult = cloudinary.Upload(uploadParams);
                b.Image = "https://res.cloudinary.com/dk65dthn5/" + uploadResult.FullyQualifiedPublicId;
                System.IO.File.Delete(_path);
            }
            
            b.BrandName = form["nameBrand"];
            b.Status = int.Parse(form["status"]);
            b.TypeProID = int.Parse(form["typePro"]);
            var data = brand.Edit(b);
            
                TempData["Success"] = "Update successful..!";
                return RedirectToAction("Index");
            }
            catch { }
                TempData["Success"] = "Update failed..!";
                return RedirectToAction("Index");
        }

        public ActionResult Details(int id)
        {
            var data = brand.GetById(id);
            ViewBag.Brand = data;
            ViewBag.Type = type_Product.GetAll().Where(x => x.Status == 1);
            return PartialView("_Details");
        }
    }
}