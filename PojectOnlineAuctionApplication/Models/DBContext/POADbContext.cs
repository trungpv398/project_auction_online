﻿using PojectOnlineAuctionApplication.Models.Entity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace PojectOnlineAuctionApplication.Models.DBContext
{
    public class POADbContext:DbContext
    {
        public POADbContext() : base("name=PojectOnlineAuction") {}

        public virtual DbSet<Category> Categories { get; set; }
        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<Type_Product> Type_Products { get; set; }
        public virtual DbSet<Brand> Brands { get; set; }
        public virtual DbSet<SalesMan> SalesMen { get; set; }
        public virtual DbSet<ProductAuction> ProductAuctions { get; set; }
        public virtual DbSet<Parameter> Parameters { get; set; }
        public virtual DbSet<ProAuParameter> ProAuParameters { get; set; }
        public virtual DbSet<Customer> Customers { get; set; }
        public virtual DbSet<Auction> Auctions { get; set; }
        public virtual DbSet<Order> Orders { get; set; }
        public virtual DbSet<TypeBanner> TypeBanners { get; set; }
        public virtual DbSet<Banner> Banners { get; set; }
        public virtual DbSet<CustomerSuport> CustomerSuports { get; set; }
        public virtual DbSet<UserManuals> UserManuals { get; set; }

        public virtual DbSet<Feedback> Feedbacks { get; set; }



    }
}