﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PojectOnlineAuctionApplication.Models.Entity
{
    public class Category
    {
        [Key]
        public int ID { get; set; }
        public string CateName { get; set; }
        public string Description { get; set; }
        public string Image { get; set; }
        public int Status { get; set; }
        public DateTime Updated { get; set; }
        public DateTime Created { get; set; }
        public IEnumerable<ProductAuction> ProductAuctions { get; set; }
    }
}