﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace PojectOnlineAuctionApplication.Models.Entity
{
    public class ProductAuction
    {
        [Key]
        public int ID { get; set; }
        public string ProName { get; set; }
        // noi san xuat
        public string Origin { get; set; }
        //gia goc
        public float OriginalPrice { get; set; }

        //gia buoc nhay
        public float PriceStep { get; set; }
        //gia mua ngay
        public float PurchasingPrice { get; set; }
        //gia khoi diem
        public float StartingPrice { get; set; }
        //image
        public string Image { get; set; }
        //mo ta san pham
        public string Description { get; set; }
        public int Status { get; set; }
        public DateTime StartDateTime { get; set; }
        public DateTime EndDateTime { get; set; }
        public int Qty { get; set; }
        public int CategoryID { get; set; }
        public int SaleManID { get; set; }
        public int BrandID { get; set; }
        //danh gia
        public int Rated { get; set; }
        public DateTime Created { get; set; }
        public DateTime Updated { get; set; }
        [ForeignKey("CategoryID")]
        public Category Category { get; set; }
        [ForeignKey("SaleManID")]
        public SalesMan SalesMan { get; set; }
        [ForeignKey("BrandID")]
        public Brand Brand { get; set; }
        public IEnumerable<ProAuParameter> ProAuParameters { get; set; }
        public IEnumerable<Action> Actions { get; set; }


    }
}