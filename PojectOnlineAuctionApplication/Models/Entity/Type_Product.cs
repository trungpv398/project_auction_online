﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PojectOnlineAuctionApplication.Models.Entity
{
    public class Type_Product
    {
        [Key]
        public int ID { get; set; }
        public string TypeProName { get; set; }
        public int Status { get; set; }
        public DateTime Created { get; set; }
        public IEnumerable<Brand> Brands { get; set; }
        public IEnumerable<Parameter> Parameters { get; set; }
    }
}