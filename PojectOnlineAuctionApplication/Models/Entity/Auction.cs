﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace PojectOnlineAuctionApplication.Models.Entity
{
    public class Auction
    {
        [Key]
        public int ID { get; set; }
        public int ProAutionID { get; set; }
        public int CustomerID { get; set; }
        public float Price { get; set; }
        public int? Status { get; set; }
        public DateTime Created { get; set; }
        [ForeignKey("ProAutionID")]
        public ProductAuction ProductAuction { get; set; }
        [ForeignKey("CustomerID")]
        public Customer Customer { get; set; }
        public IEnumerable<Order> Orders { get; set; }
    }
}