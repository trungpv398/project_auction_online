﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PojectOnlineAuctionApplication.Models.Entity
{
    public class SalesMan
    {
        [Key]
        public int ID { get; set; }
        public string FullName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string Address { get; set; }
        public int Status { get; set; }
        public string CMND { get; set; }
        public string Avatar { get; set; }
        public int? Rate { get; set; }
        public string PhoneNumber { get; set; }
        public DateTime Created { get; set; }
        public IEnumerable<ProductAuction> ProductAuctions { get; set; }

    }
}