﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace PojectOnlineAuctionApplication.Models.Entity
{
    public class Banner
    {
        [Key]
        public int ID { get; set; }
        public int TypeBannerID { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Image { get; set; }
        public int Status { get; set; }
        public DateTime Created { get; set; }
        [ForeignKey("TypeBannerID")]
        public TypeBanner TypeBanner { get; set; }




    }
}