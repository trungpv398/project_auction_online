﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace PojectOnlineAuctionApplication.Models.Entity
{
    public class Parameter
    {
        [Key]
        public int ID { get; set; }
        public string ParameterName { get; set; }
        public int Status { get; set; }
        public int TypeProID { get; set; }
        public DateTime Created { get; set; }
        [ForeignKey("TypeProID")]
        public Type_Product Type_Product { get; set; }
        public IEnumerable<ProAuParameter> ProAuParameters { get; set; }
    }
}