﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace PojectOnlineAuctionApplication.Models.Entity
{
    public class ProAuParameter
    {
        [Key]
        public int ID { get; set; }
        public int? ProAuctionID { get; set; }
        public int ParameterID { get; set; }
        public string ParameterName { get; set; }
        public string Value { get; set; }
        public DateTime Created { get; set; }
        [ForeignKey("ProAuctionID")]
        public ProductAuction ProductAuction { get; set; }
        [ForeignKey("ParameterID")]
        public Parameter Parameter { get; set; }
    }
}