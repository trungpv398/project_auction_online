﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace PojectOnlineAuctionApplication.Models.Entity
{
    public class Order
    {
        [Key]
        public int ID { get; set; }
        public int AutionID { get; set; }
        //ten nguoi nhan
        public string Receiver { get; set; }
        public int CustomerID { get; set; }
        public int Payment { get; set; }
        public string Email { get; set; }
        public string Note { get; set; }
        public int status { get; set; }
        public string Phone { get; set; }
        public DateTime Created { get; set; }
        [ForeignKey("AutionID")]
        public Auction Auction { get; set; }

        public DateTime EndDateTime { get; set; }
        public float TotalPrice { get; set; }
        public float AuctionFees { get; set; }
        public float MoneyReceived{get;set;}
        public int? ProAutionID { get; set; }
    }
}