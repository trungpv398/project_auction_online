﻿using PojectOnlineAuctionApplication.Controllers.Repository;
using PojectOnlineAuctionApplication.Models.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace PojectOnlineAuctionApplication.Controllers.Api
{
    public class SupportController : ApiController
    {
        IRepository<CustomerSuport> support;
        public SupportController()
        {
            support = new Repository<CustomerSuport>();
        }

        [HttpGet]
        public IEnumerable<CustomerSuport> Get()
        {
            return support.Get();
        }

        

        [HttpGet]
        public CustomerSuport Get(int id)
        {
            return support.Get(id);
        }

        [HttpPost]
        public IHttpActionResult Post(CustomerSuport c)
        {           
            if(support.Get(x=>x.Title==c.Title).Count()>0)
            {
                ModelState.AddModelError("Title", "Tiêu đề bài viết đã tồn tại");
            }
            if(ModelState.IsValid)
            {
                support.Add(c);
                return Content(HttpStatusCode.OK, new Message
                {
                    StatusCode = 200,
                    Messages = "Thành công!",
                   

                });
            }
            Dictionary<string, string> erros = new Dictionary<string, string>();
            //doc ra loi trong ModelsState
            foreach (var key in ModelState.Keys)
            {
                foreach (var item in ModelState[key].Errors)
                {
                    erros.Add(key, item.ErrorMessage);
                }
            }
            return Content(HttpStatusCode.OK, new Message
            {
                StatusCode = 220,
                Messages = "Dữ liệu không hợp lệ!",               
            });
        }

        [HttpPut]
        public CustomerSuport Put(CustomerSuport c)
        {
            support.Edit(c);
            return c;
        }

        [HttpDelete]
        public CustomerSuport Delete(int id)
        {
            var data = support.Get(id);
            support.Remove(id);
            return data;
        }
    }
}
