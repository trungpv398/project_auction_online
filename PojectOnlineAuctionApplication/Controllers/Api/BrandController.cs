﻿using PojectOnlineAuctionApplication.Controllers.Repository;
using PojectOnlineAuctionApplication.Models.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace PojectOnlineAuctionApplication.Controllers.Api
{
    public class BrandController : ApiController
    {
        IRepository<Brand> brand;
        public BrandController()
        {
            brand = new Repository<Brand>();
        }

        [HttpGet]
        public IEnumerable<Brand> Get()
        {
            return brand.Get();
        }

        [HttpGet]
        public IEnumerable<Brand> GetByTypePro(int typePro)
        {
            return brand.Get().Where(x=>x.TypeProID == typePro);
        }

        [HttpGet]
        public Brand Get(int id)
        {
            return brand.Get(id);
        }

        [HttpPost]
        public IHttpActionResult Post(Brand c)
        {
            if(brand.Get(x=>x.BrandName == c.BrandName).Count()>0)
            {
                ModelState.AddModelError("", "Tên thương hiêu đã tồn tại");
            }
            if(ModelState.IsValid)
            {
                brand.Add(c);
                return Content(HttpStatusCode.OK, new Message
                {
                    StatusCode = 200,
                    Messages = "Thành Công",
                });

            }
            Dictionary<string, string> erros = new Dictionary<string, string>();
            //doc ra loi trong ModelsState
            foreach (var key in ModelState.Keys)
            {
                foreach (var item in ModelState[key].Errors)
                {
                    erros.Add(key, item.ErrorMessage);
                }
            }
            return Content(HttpStatusCode.OK, new Message
            {
                StatusCode = 220,
                Messages = "Dữ liệu không hợp lệ!",

            });
           
        }

        [HttpPut]
        public Brand Put(Brand c)
        {
            brand.Edit(c);
            return c;
        }

        [HttpDelete]
        public Brand Delete(int id)
        {
            var data = brand.Get(id);
            brand.Remove(id);
            return data;
        }
    }
}
