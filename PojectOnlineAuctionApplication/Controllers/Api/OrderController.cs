﻿using PojectOnlineAuctionApplication.Controllers.Repository;
using PojectOnlineAuctionApplication.Models.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace PojectOnlineAuctionApplication.Controllers.Api
{
    public class OrderController : ApiController
    {
        IRepository<Order> order;
        public OrderController()
        {
            order = new Repository<Order>();
        }

        [HttpGet]
        public IEnumerable<Order> Get()
        {
            return order.Get();
        }

        [HttpGet]
        public IEnumerable<Order> GetByAution(int autionID)
        {
            return order.Get().Where(x=>x.AutionID==autionID);
        }

        [HttpGet]
        public IEnumerable<Order> GetByCustomer(int customerID)
        {
            return order.Get().Where(x => x.CustomerID == customerID);
        }

        [HttpGet]
        public Order Get(int id)
        {
            return order.Get(id);
        }

        [HttpPost]
        public IHttpActionResult Post(Order c)
        {
            order.Add(c);
            return Content(HttpStatusCode.OK, new Message
            {
                StatusCode = 200,
                Messages = "Thành Công",
            });
        }

        [HttpPut]
        public Order Put(Order c)
        {
            order.Edit(c);
            return c;
        }

        [HttpDelete]
        public Order Delete(int id)
        {
            var data = order.Get(id);
            order.Remove(id);
            return data;
        }
    }
}
