﻿using PojectOnlineAuctionApplication.Controllers.Repository;
using PojectOnlineAuctionApplication.Models.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Cryptography;
using System.Text;
using System.Web.Http;

namespace PojectOnlineAuctionApplication.Controllers.Api
{
    public class CustomerController : ApiController
    {
        IRepository<Customer> customer;
        public CustomerController()
        {
            customer = new Repository<Customer>();
        }

        [HttpGet]
        public IEnumerable<Customer> Get()
        {
            return customer.Get();
        }
        [HttpGet]
        public Customer GetCustomer(string email,string pwd)
        {
            string pass = GETSHA(pwd);
            var data = customer.Get();
            foreach(Customer item in data)
            {
                if(item.Email == email && item.Password == pass)
                {                
                   return customer.Get().Where(x => x.Email == email).First(); ;                   
                }
            }

            return null;
        }

        [HttpGet]
        public Customer GetByEmail(string email)
        {
            return customer.Get().Where(x => x.Email == email).First();
        }

        [HttpGet]
        public Customer Get(int id)
        {
            return customer.Get(id);
        }

        [HttpPost]
        public IHttpActionResult Post(Customer c)
        {
            if(customer.Get(x=>x.Email == c.Email).Count()>0)
            {
                ModelState.AddModelError("Email", "Email da ton tai");
            }
            if(ModelState.IsValid)
            {
                c.Password = GETSHA(c.Password);
                customer.Add(c);
                return Content(HttpStatusCode.OK, new Message
                {
                    StatusCode = 200,
                    Messages = "Thành Công",
                });
            }
            Dictionary<string, string> erros = new Dictionary<string, string>();
            //doc ra loi trong ModelsState
            foreach (var key in ModelState.Keys)
            {
                foreach (var item in ModelState[key].Errors)
                {
                    erros.Add(key, item.ErrorMessage);
                }
            }
            return Content(HttpStatusCode.OK, new Message
            {
                StatusCode = 220,
                Messages = "Dữ liệu không hợp lệ!",

            });
        }

        [HttpPut]
        public Customer Put(Customer c)
        {
            /*c.Password = GETSHA(c.Password);*/
            customer.Edit(c);
            return c;
        }

        [HttpDelete]
        public Customer Delete(int id)
        {
            var data = customer.Get(id);
            customer.Remove(id);
            return data;
        }
        public static string GETSHA(string str)
        {
            /*MD5 md5 = new MD5CryptoServiceProvider();*/
            SHA512 sha = new SHA512CryptoServiceProvider();
            byte[] fromData = Encoding.UTF8.GetBytes(str);
            byte[] targetData = sha.ComputeHash(fromData);
            string byte2String = null;

            for (int i = 0; i < targetData.Length; i++)
            {
                byte2String += targetData[i].ToString("x2");

            }
            return byte2String;
        }
    }
}
