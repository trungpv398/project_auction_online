﻿using PojectOnlineAuctionApplication.Controllers.Repository;
using PojectOnlineAuctionApplication.Models.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace PojectOnlineAuctionApplication.Controllers.Api
{
    public class CategoryController : ApiController
    {
        IRepository<Category> cate;
        public CategoryController()
        {
            cate = new Repository<Category>();
        }

        [HttpGet]
        public IEnumerable<Category> Get()
        {
            return cate.Get();
        }

        [HttpGet]
        public Category Get(int id)
        {
            return cate.Get(id);
        }

        [HttpPost]
        public Category Post(Category c)
        {
            cate.Add(c);
            return c;
        }

        [HttpPut]
        public Category Put(Category c)
        {
            cate.Edit(c);
            return c;
        }

        [HttpDelete]
        public Category Delete(int id)
        {
            var data = cate.Get(id);
            cate.Remove(id);
            return data;
        }




    }
}
