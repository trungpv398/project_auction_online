﻿using PojectOnlineAuctionApplication.Controllers.Repository;
using PojectOnlineAuctionApplication.Models.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace PojectOnlineAuctionApplication.Controllers.Api
{
    public class ProAu_ParameterController : ApiController
    {
        IRepository<ProAuParameter> proAuParam;
        public ProAu_ParameterController()
        {
            proAuParam = new Repository<ProAuParameter>();
        }

        [HttpGet]
        public IEnumerable<ProAuParameter> Get()
        {
            return proAuParam.Get();
        }

        [HttpGet]
        public IEnumerable<ProAuParameter> GetByProAuction(int proAuctionId)
        {
            return proAuParam.Get().Where(x => x.ProAuctionID == proAuctionId);
        }


        [HttpGet]
        public ProAuParameter Get(int id)
        {
            return proAuParam.Get(id);
        }

        [HttpPost]
        public ProAuParameter Post(ProAuParameter c)
        {
            proAuParam.Add(c);
            return c;
        }

        [HttpPut]
        public ProAuParameter Put(ProAuParameter c)
        {
            proAuParam.Edit(c);
            return c;
        }

        [HttpDelete]
        public ProAuParameter Delete(int id)
        {
            var data = proAuParam.Get(id);
            proAuParam.Remove(id);
            return data;
        }
    }
}
