﻿using PojectOnlineAuctionApplication.Controllers.Repository;
using PojectOnlineAuctionApplication.Models.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace PojectOnlineAuctionApplication.Controllers.Api
{
    public class BannerController : ApiController
    {
        IRepository<Banner> banner;
        public BannerController()
        {
            banner = new Repository<Banner>();
        }

        [HttpGet]
        public IEnumerable<Banner> Get()
        {
            return banner.Get();
        }

        [HttpGet]
        public IEnumerable<Banner> GetByType(int typeBanner)
        {
            return banner.Get().Where(x => x.TypeBannerID == typeBanner);
        }


        [HttpGet]
        public Banner Get(int id)
        {
            return banner.Get(id);
        }

        [HttpPost]
        public IHttpActionResult Post(Banner c)
        {
            banner.Add(c);
            return Content(HttpStatusCode.OK, new Message
            {
                StatusCode = 200,
                Messages = "Thành công!",
            });
        }

        [HttpPut]
        public Banner Put(Banner c)
        {
            banner.Edit(c);
            return c;
        }

        [HttpDelete]
        public Banner Delete(int id)
        {
            var data = banner.Get(id);
            banner.Remove(id);
            return data;
        }

    }
}
