﻿using PojectOnlineAuctionApplication.Controllers.Repository;
using PojectOnlineAuctionApplication.Models.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace PojectOnlineAuctionApplication.Controllers.Api
{
    public class FeedBackController : ApiController
    {
        IRepository<Feedback> feedback;
        public FeedBackController()
        {
            feedback = new Repository<Feedback>();
        }
        [HttpGet]
        public IEnumerable<Feedback> Get()
        {
            return feedback.Get();
        }

        [HttpGet]
        public Feedback Get(int id)
        {
            return feedback.Get(id);
        }
        [HttpPost]
        public Feedback Post(Feedback c)
        {
            feedback.Add(c);
            return c;
        }
        [HttpPut]
        public Feedback Put(Feedback c)
        {
            feedback.Edit(c);
            return c;
        }
    }
}
