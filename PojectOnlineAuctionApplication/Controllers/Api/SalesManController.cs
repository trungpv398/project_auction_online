﻿using PojectOnlineAuctionApplication.Controllers.Repository;
using PojectOnlineAuctionApplication.Models.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace PojectOnlineAuctionApplication.Controllers.Api
{
    public class SalesManController : ApiController
    {
        IRepository<SalesMan> saleMan;
        public SalesManController()
        {
            saleMan = new Repository<SalesMan>();
        }

        [HttpGet]
        public IEnumerable<SalesMan> Get()
        {
            return saleMan.Get();
        }


        [HttpGet]
        public SalesMan Get(int id)
        {
            return saleMan.Get(id);
        }

        [HttpPost]
        public IHttpActionResult Post(SalesMan c)
        {
            if(saleMan.Get(x=>x.Email == c.Email).Count()>0)
            {
                ModelState.AddModelError("Email", "Email da ton tai");
            }
            if(ModelState.IsValid)
            {
                saleMan.Add(c);
                return Content(HttpStatusCode.OK, new Message
                {
                    StatusCode = 200,
                    Messages = "Thành Công",
                });
            }
            Dictionary<string, string> erros = new Dictionary<string, string>();
            //doc ra loi trong ModelsState
            foreach (var key in ModelState.Keys)
            {
                foreach (var item in ModelState[key].Errors)
                {
                    erros.Add(key, item.ErrorMessage);
                }
            }
            return Content(HttpStatusCode.OK, new Message
            {
                StatusCode = 220,
                Messages = "Dữ liệu không hợp lệ!",

            });
        }

        [HttpPut]
        public SalesMan Put(SalesMan c)
        {
            saleMan.Edit(c);
            return c;
        }

        [HttpDelete]
        public SalesMan Delete(int id)
        {
            var data = saleMan.Get(id);
            saleMan.Remove(id);
            return data;
        }

    }
}
