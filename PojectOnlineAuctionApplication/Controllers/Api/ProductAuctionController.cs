﻿using PojectOnlineAuctionApplication.Controllers.Repository;
using PojectOnlineAuctionApplication.Models.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace PojectOnlineAuctionApplication.Controllers.Api
{
    public class ProductAuctionController : ApiController
    {
        IRepository<ProductAuction> proAuction;
        public ProductAuctionController()
        {
            proAuction = new Repository<ProductAuction>();
        }

        [HttpGet]
        public IEnumerable<ProductAuction> Get()
        {
            return proAuction.Get();
        }

        [HttpGet]
        public IEnumerable<ProductAuction> GetByCate(int cateId)
        {
            return proAuction.Get().Where(x => x.CategoryID == cateId);
        }

        [HttpGet]
        public IEnumerable<ProductAuction> GetByStatus(int status)
        {
            return proAuction.Get().Where(x => x.Status == status);
        }

        [HttpGet]
        public IEnumerable<ProductAuction> GetBybrand(int brandId)
        {
            return proAuction.Get().Where(x => x.BrandID == brandId);
        }

        [HttpGet]
        public IEnumerable<ProductAuction> GetBySaleMan(int salemanId)
        {
            return proAuction.Get().Where(x => x.SaleManID == salemanId);
        }

        [HttpGet]
        public ProductAuction Get(int id)
        {
            return proAuction.Get(id);
        }

        [HttpPost]
        public ProductAuction Post(ProductAuction c)
        {
            proAuction.Add(c);
            return c;
        }

        [HttpPut]
        public ProductAuction Put(ProductAuction c)
        {
            proAuction.Edit(c);
            return c;
        }

        [HttpDelete]
        public ProductAuction Delete(int id)
        {
            var data = proAuction.Get(id);
            proAuction.Remove(id);
            return data;
        }
    }
}
