﻿using PojectOnlineAuctionApplication.Controllers.Repository;
using PojectOnlineAuctionApplication.Models.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Cryptography;
using System.Text;
using System.Web.Http;

namespace PojectOnlineAuctionApplication.Controllers.Api
{
    public class UserController : ApiController
    {
        IRepository<User> user;
        public UserController()
        {
            user = new Repository<User>();
        }

        [HttpGet]
        public IEnumerable<User> Get()
        {
            return user.Get();
        }
        [HttpGet]
        public User GetCustomer(string email, string pwd)
        {
            string pass = GETSHA(pwd);
            var data = user.Get();
            foreach (User item in data)
            {
                if (item.Email == email && item.Password == pass)
                {

                    return user.Get().Where(x => x.Email == email).First(); ;

                }
            }

            return null;
        }

        [HttpGet]
        public User GetByEmail(string email)
        {
            return user.Get().Where(x => x.Email == email).First();
        }

        [HttpGet]
        public User Get(int id)
        {
            return user.Get(id);
        }

        [HttpPost]
        public IHttpActionResult Post(User c)
        {
            if(user.Get(x=>x.Email == c.Email).Count() > 0)
            {
                ModelState.AddModelError("Email","Emai da ton tai");
            }
            if(ModelState.IsValid)
            {
                c.Password = GETSHA(c.Password);
                user.Add(c);
                return Content(HttpStatusCode.OK, new Message
                {
                    StatusCode = 200,
                    Messages = "Thành công!",
                });
            }
            Dictionary<string, string> erros = new Dictionary<string, string>();
            //doc ra loi trong ModelsState
            foreach (var key in ModelState.Keys)
            {
                foreach (var item in ModelState[key].Errors)
                {
                    erros.Add(key, item.ErrorMessage);
                }
            }
            return Content(HttpStatusCode.OK, new Message
            {
                StatusCode = 220,
                Messages = "Dữ liệu không hợp lệ!",

            });

        }

        [HttpPut]
        public User Put(User c)
        {
            c.Password = GETSHA(c.Password);
            user.Edit(c);
            return c;
        }

        [HttpDelete]
        public User Delete(int id)
        {
            var data = user.Get(id);
            user.Remove(id);
            return data;
        }
        public static string GETSHA(string str)
        {
            /*MD5 md5 = new MD5CryptoServiceProvider();*/
            SHA512 sha = new SHA512CryptoServiceProvider();
            byte[] fromData = Encoding.UTF8.GetBytes(str);
            byte[] targetData = sha.ComputeHash(fromData);
            string byte2String = null;

            for (int i = 0; i < targetData.Length; i++)
            {
                byte2String += targetData[i].ToString("x2");

            }
            return byte2String;
        }

    }
}
