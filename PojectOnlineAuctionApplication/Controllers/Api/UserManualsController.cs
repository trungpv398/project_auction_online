﻿using PojectOnlineAuctionApplication.Controllers.Repository;
using PojectOnlineAuctionApplication.Models.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace PojectOnlineAuctionApplication.Controllers.Api
{
    public class UserManualsController : ApiController
    {
        IRepository<UserManuals> userManualas;
        public UserManualsController()
        {
            userManualas = new Repository<UserManuals>();
        }

        [HttpGet]
        public IEnumerable<UserManuals> Get()
        {
            return userManualas.Get();
        }
     
        [HttpGet]
        public UserManuals Get(int id)
        {
            return userManualas.Get(id);
        }

        [HttpPost]
        public IHttpActionResult Post(UserManuals c)
        {
            if(userManualas.Get(x => x.Title == c.Title).Count()>0)
            {
                ModelState.AddModelError("Title","Tiêu đề bài viết đã tồn tại");
            }
            if(ModelState.IsValid)
            {
                userManualas.Add(c);
                return Content(HttpStatusCode.OK, new Message
                {
                    StatusCode = 200,
                    Messages = "Thành công!",


                });
            }
            Dictionary<string, string> erros = new Dictionary<string, string>();
            //doc ra loi trong ModelsState
            foreach (var key in ModelState.Keys)
            {
                foreach (var item in ModelState[key].Errors)
                {
                    erros.Add(key, item.ErrorMessage);
                }
            }
            return Content(HttpStatusCode.OK, new Message
            {
                StatusCode = 220,
                Messages = "Dữ liệu không hợp lệ!",
            });
        }

        [HttpPut]
        public UserManuals Put(UserManuals c)
        {
            userManualas.Edit(c);
            return c;
        }

        [HttpDelete]
        public UserManuals Delete(int id)
        {
            var data = userManualas.Get(id);
            userManualas.Remove(id);
            return data;
        }

    }
}
