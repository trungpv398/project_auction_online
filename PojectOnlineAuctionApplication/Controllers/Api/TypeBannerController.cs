﻿using PojectOnlineAuctionApplication.Controllers.Repository;
using PojectOnlineAuctionApplication.Models.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace PojectOnlineAuctionApplication.Controllers.Api
{
    public class TypeBannerController : ApiController
    {
        IRepository<TypeBanner> typeBanner;
        public TypeBannerController()
        {
            typeBanner = new Repository<TypeBanner>();
        }

        [HttpGet]
        public IEnumerable<TypeBanner> Get()
        {
            return typeBanner.Get();
        }

       

        [HttpGet]
        public TypeBanner Get(int id)
        {
            return typeBanner.Get(id);
        }

        [HttpPost]
        public IHttpActionResult Post(TypeBanner c)
        {
            typeBanner.Add(c);
            return Content(HttpStatusCode.OK, new Message
            {
                StatusCode = 200,
                Messages = "Thành công!",


            });
        }

        [HttpPut]
        public TypeBanner Put(TypeBanner c)
        {
            typeBanner.Edit(c);
            return c;
        }

        [HttpDelete]
        public TypeBanner Delete(int id)
        {
            var data = typeBanner.Get(id);
            typeBanner.Remove(id);
            return data;
        }
    }
}
