﻿using PojectOnlineAuctionApplication.Controllers.Repository;
using PojectOnlineAuctionApplication.Models.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace PojectOnlineAuctionApplication.Controllers.Api
{
    public class ParameterController : ApiController
    {
        IRepository<Parameter> parameter;
        public ParameterController()
        {
            parameter = new Repository<Parameter>();
        }

        [HttpGet]
        public IEnumerable<Parameter> Get()
        {
            return parameter.Get();
        }

        [HttpGet]
        public IEnumerable<Parameter> GetByType(int typeProId)
        {
            return parameter.Get().Where(x => x.TypeProID == typeProId);
        }


        [HttpGet]
        public Parameter Get(int id)
        {
            return parameter.Get(id);
        }

        [HttpPost]
        public IHttpActionResult Post(Parameter c)
        {
            if(parameter.Get(x=>x.ParameterName == c.ParameterName).Count()>0)
            {
                ModelState.AddModelError("ParameterName", "Tên không được trùng lặp..");
            }
            if(ModelState.IsValid)
            {
                parameter.Add(c);
                return Content(HttpStatusCode.OK, new Message
                {
                    StatusCode = 200,
                    Messages = "Thành công!",


                });
            }
            Dictionary<string, string> erros = new Dictionary<string, string>();
            //doc ra loi trong ModelsState
            foreach (var key in ModelState.Keys)
            {
                foreach (var item in ModelState[key].Errors)
                {
                    erros.Add(key, item.ErrorMessage);
                }
            }
            return Content(HttpStatusCode.OK, new Message
            {
                StatusCode = 220,
                Messages = "Dữ liệu không hợp lệ!",
                
            });
        }

        [HttpPut]
        public Parameter Put(Parameter c)
        {
            parameter.Edit(c);
            return c;
        }

        [HttpDelete]
        public Parameter Delete(int id)
        {
            var data = parameter.Get(id);
            parameter.Remove(id);
            return data;
        }
    }
}
