﻿using PojectOnlineAuctionApplication.Controllers.Repository;
using PojectOnlineAuctionApplication.Models.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace PojectOnlineAuctionApplication.Controllers.Api
{
    public class TypeProductController : ApiController
    {
        IRepository<Type_Product> typePro;
        public TypeProductController()
        {
            typePro = new Repository<Type_Product>();
        }

        [HttpGet]
        public IEnumerable<Type_Product> Get()
        {
            return typePro.Get();
        }

        [HttpGet]
        public Type_Product Get(int id)
        {
            return typePro.Get(id);
        }

        [HttpPost]
        public IHttpActionResult Post(Type_Product c)
        {
            if(typePro.Get(x => x.TypeProName == c.TypeProName).Count()>0)
            {
                ModelState.AddModelError("TypeProName","Ten da ton tai");
            }
            if(ModelState.IsValid)
            {
                typePro.Add(c);
                return Content(HttpStatusCode.OK, new
                {
                    StatusCode = 200,
                    Message = "Thanh cong!",
                    Data = c

                });
            }
            Dictionary<string, string> erros = new Dictionary<string, string>();
            //doc ra loi trong ModelsState
            foreach(var key in ModelState.Keys)
            {
                foreach(var item in ModelState[key].Errors)
                {
                    erros.Add(key, item.ErrorMessage);
                }
            }
            return Content(HttpStatusCode.OK, new
            {
                StatusCode = 220,
                Message = "Du lieu khong hop le!",
                Data = erros

            });
        }

        [HttpPut]
        public Type_Product Put(Type_Product c)
        {
            typePro.Edit(c);
            return c;
        }

        [HttpDelete]
        public Type_Product Delete(int id)
        {
            var data = typePro.Get(id);
            typePro.Remove(id);
            return data;
        }
    }
}
