﻿using PojectOnlineAuctionApplication.Controllers.Repository;
using PojectOnlineAuctionApplication.Models.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace PojectOnlineAuctionApplication.Controllers.Api
{
    public class AuctionController : ApiController
    {
        IRepository<Auction> auction;
        public AuctionController()
        {
            auction = new Repository<Auction>();
        }

        [HttpGet]
        public IEnumerable<Auction> Get()
        {
            return auction.Get();
        }

        [HttpGet]
        public IEnumerable<Auction> GetByCustomer(int customerID)
        {
            return auction.Get().Where(x => x.CustomerID == customerID);
        }

        [HttpGet]
        public IEnumerable<Auction> GetByProduct(int proAuctionID)
        {
            return auction.Get().Where(x => x.ProAutionID == proAuctionID);
        }


        [HttpGet]
        public Auction Get(int id)
        {
            return auction.Get(id);
        }

        [HttpPost]
        public Auction Post(Auction c)
        {
            auction.Add(c);
            return c;
        }

        [HttpPut]
        public Auction Put(Auction c)
        {
            auction.Edit(c);
            return c;
        }

        [HttpDelete]
        public Auction Delete(int id)
        {
            var data = auction.Get(id);
            auction.Remove(id);
            return data;
        }
    }
}
