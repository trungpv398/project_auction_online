﻿namespace PojectOnlineAuctionApplication.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _12242020_edit_order : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Orders", "CustomerID", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Orders", "CustomerID");
        }
    }
}
