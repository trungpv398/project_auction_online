﻿namespace PojectOnlineAuctionApplication.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _01072021_update_data_status_auction : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Auctions", "Status", c => c.Int());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Auctions", "Status");
        }
    }
}
