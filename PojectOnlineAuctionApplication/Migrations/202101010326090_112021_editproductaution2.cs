﻿namespace PojectOnlineAuctionApplication.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _112021_editproductaution2 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ProductAuctions", "Status", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.ProductAuctions", "Status");
        }
    }
}
