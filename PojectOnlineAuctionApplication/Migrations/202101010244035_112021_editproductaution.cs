﻿namespace PojectOnlineAuctionApplication.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _112021_editproductaution : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ProductAuctions", "Image", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.ProductAuctions", "Image");
        }
    }
}
