﻿namespace PojectOnlineAuctionApplication.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _01142021_addcolum_dborder : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Orders", "AuctionFees", c => c.Single(nullable: false));
            AddColumn("dbo.Orders", "MoneyReceived", c => c.Single(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Orders", "MoneyReceived");
            DropColumn("dbo.Orders", "AuctionFees");
        }
    }
}
