﻿namespace PojectOnlineAuctionApplication.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _112021_update_database_proAuction : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ProductAuctions", "StartingPrice", c => c.Single(nullable: false));
            AddColumn("dbo.ProductAuctions", "Description", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.ProductAuctions", "Description");
            DropColumn("dbo.ProductAuctions", "StartingPrice");
        }
    }
}
