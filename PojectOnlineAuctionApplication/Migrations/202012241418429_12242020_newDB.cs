﻿namespace PojectOnlineAuctionApplication.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _12242020_newDB : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Auctions",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        ProAutionID = c.Int(nullable: false),
                        CustomerID = c.Int(nullable: false),
                        Price = c.Single(nullable: false),
                        Created = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Customers", t => t.CustomerID, cascadeDelete: true)
                .ForeignKey("dbo.ProductAuctions", t => t.ProAutionID, cascadeDelete: true)
                .Index(t => t.ProAutionID)
                .Index(t => t.CustomerID);
            
            CreateTable(
                "dbo.Customers",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        FullName = c.String(),
                        Email = c.String(),
                        Password = c.String(),
                        Address = c.String(),
                        Status = c.Int(nullable: false),
                        CMND = c.String(),
                        PhoneNumber = c.String(),
                        Avatar = c.String(),
                        Rate = c.Int(),
                        Created = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.ProductAuctions",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        ProName = c.String(),
                        Origin = c.String(),
                        OriginalPrice = c.Single(nullable: false),
                        StartDateTime = c.DateTime(nullable: false),
                        EndDateTime = c.DateTime(nullable: false),
                        Qty = c.Int(nullable: false),
                        CategoryID = c.Int(nullable: false),
                        SaleManID = c.Int(nullable: false),
                        BrandID = c.Int(nullable: false),
                        Created = c.DateTime(nullable: false),
                        Updated = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Brands", t => t.BrandID, cascadeDelete: true)
                .ForeignKey("dbo.Categories", t => t.CategoryID, cascadeDelete: true)
                .ForeignKey("dbo.SalesMen", t => t.SaleManID, cascadeDelete: true)
                .Index(t => t.CategoryID)
                .Index(t => t.SaleManID)
                .Index(t => t.BrandID);
            
            CreateTable(
                "dbo.Brands",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        BrandName = c.String(),
                        Image = c.String(),
                        Status = c.Int(nullable: false),
                        TypeProID = c.Int(nullable: false),
                        Created = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Type_Product", t => t.TypeProID, cascadeDelete: true)
                .Index(t => t.TypeProID);
            
            CreateTable(
                "dbo.Type_Product",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        TypeProName = c.String(),
                        Status = c.Int(nullable: false),
                        Created = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Categories",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        CateName = c.String(),
                        Description = c.String(),
                        Image = c.String(),
                        Status = c.Int(nullable: false),
                        Updated = c.DateTime(nullable: false),
                        Created = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.SalesMen",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        FullName = c.String(),
                        Email = c.String(),
                        Password = c.String(),
                        Address = c.String(),
                        Status = c.Int(nullable: false),
                        CMND = c.String(),
                        Avatar = c.String(),
                        Rate = c.Int(),
                        PhoneNumber = c.String(),
                        Created = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Banners",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        TypeBannerID = c.Int(nullable: false),
                        Title = c.String(),
                        Description = c.String(),
                        Image = c.String(),
                        Status = c.Int(nullable: false),
                        Created = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.TypeBanners", t => t.TypeBannerID, cascadeDelete: true)
                .Index(t => t.TypeBannerID);
            
            CreateTable(
                "dbo.TypeBanners",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Title = c.String(),
                        Description = c.String(),
                        Status = c.Int(nullable: false),
                        Created = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.CustomerSuports",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Title = c.String(),
                        Content = c.String(),
                        Status = c.Int(nullable: false),
                        Created = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Orders",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        AutionID = c.Int(nullable: false),
                        Receiver = c.String(),
                        Payment = c.Int(nullable: false),
                        Email = c.String(),
                        Note = c.String(),
                        status = c.Int(nullable: false),
                        Phone = c.String(),
                        Created = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Auctions", t => t.AutionID, cascadeDelete: true)
                .Index(t => t.AutionID);
            
            CreateTable(
                "dbo.Parameters",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        ParameterName = c.String(),
                        Status = c.Int(nullable: false),
                        TypeProID = c.Int(nullable: false),
                        Created = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Type_Product", t => t.TypeProID, cascadeDelete: true)
                .Index(t => t.TypeProID);
            
            CreateTable(
                "dbo.ProAuParameters",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        ProAuctionID = c.Int(),
                        ParameterID = c.Int(nullable: false),
                        ParameterName = c.String(),
                        Value = c.String(),
                        Created = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Parameters", t => t.ParameterID, cascadeDelete: true)
                .ForeignKey("dbo.ProductAuctions", t => t.ProAuctionID)
                .Index(t => t.ProAuctionID)
                .Index(t => t.ParameterID);
            
            CreateTable(
                "dbo.UserManuals",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Title = c.String(),
                        Content = c.String(),
                        Status = c.Int(nullable: false),
                        Created = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        FullName = c.String(),
                        Email = c.String(),
                        Password = c.String(),
                        Address = c.String(),
                        Status = c.Int(nullable: false),
                        CMND = c.String(),
                        PhoneNumber = c.String(),
                        Avatar = c.String(),
                        Created = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ProAuParameters", "ProAuctionID", "dbo.ProductAuctions");
            DropForeignKey("dbo.ProAuParameters", "ParameterID", "dbo.Parameters");
            DropForeignKey("dbo.Parameters", "TypeProID", "dbo.Type_Product");
            DropForeignKey("dbo.Orders", "AutionID", "dbo.Auctions");
            DropForeignKey("dbo.Banners", "TypeBannerID", "dbo.TypeBanners");
            DropForeignKey("dbo.Auctions", "ProAutionID", "dbo.ProductAuctions");
            DropForeignKey("dbo.ProductAuctions", "SaleManID", "dbo.SalesMen");
            DropForeignKey("dbo.ProductAuctions", "CategoryID", "dbo.Categories");
            DropForeignKey("dbo.ProductAuctions", "BrandID", "dbo.Brands");
            DropForeignKey("dbo.Brands", "TypeProID", "dbo.Type_Product");
            DropForeignKey("dbo.Auctions", "CustomerID", "dbo.Customers");
            DropIndex("dbo.ProAuParameters", new[] { "ParameterID" });
            DropIndex("dbo.ProAuParameters", new[] { "ProAuctionID" });
            DropIndex("dbo.Parameters", new[] { "TypeProID" });
            DropIndex("dbo.Orders", new[] { "AutionID" });
            DropIndex("dbo.Banners", new[] { "TypeBannerID" });
            DropIndex("dbo.Brands", new[] { "TypeProID" });
            DropIndex("dbo.ProductAuctions", new[] { "BrandID" });
            DropIndex("dbo.ProductAuctions", new[] { "SaleManID" });
            DropIndex("dbo.ProductAuctions", new[] { "CategoryID" });
            DropIndex("dbo.Auctions", new[] { "CustomerID" });
            DropIndex("dbo.Auctions", new[] { "ProAutionID" });
            DropTable("dbo.Users");
            DropTable("dbo.UserManuals");
            DropTable("dbo.ProAuParameters");
            DropTable("dbo.Parameters");
            DropTable("dbo.Orders");
            DropTable("dbo.CustomerSuports");
            DropTable("dbo.TypeBanners");
            DropTable("dbo.Banners");
            DropTable("dbo.SalesMen");
            DropTable("dbo.Categories");
            DropTable("dbo.Type_Product");
            DropTable("dbo.Brands");
            DropTable("dbo.ProductAuctions");
            DropTable("dbo.Customers");
            DropTable("dbo.Auctions");
        }
    }
}
