﻿namespace PojectOnlineAuctionApplication.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _28122020_updatedatabase : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ProductAuctions", "PriceStep", c => c.Single(nullable: false));
            AddColumn("dbo.ProductAuctions", "PurchasingPrice", c => c.Single(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.ProductAuctions", "PurchasingPrice");
            DropColumn("dbo.ProductAuctions", "PriceStep");
        }
    }
}
