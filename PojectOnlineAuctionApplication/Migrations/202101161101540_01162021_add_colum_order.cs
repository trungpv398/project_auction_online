﻿namespace PojectOnlineAuctionApplication.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _01162021_add_colum_order : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Orders", "ProAutionID", c => c.Int());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Orders", "ProAutionID");
        }
    }
}
