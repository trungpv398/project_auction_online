﻿namespace PojectOnlineAuctionApplication.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _01122021_add_endedatetime_dborder : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Orders", "EndDateTime", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Orders", "EndDateTime");
        }
    }
}
