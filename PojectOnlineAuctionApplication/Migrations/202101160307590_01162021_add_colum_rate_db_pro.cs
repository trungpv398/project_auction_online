﻿namespace PojectOnlineAuctionApplication.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _01162021_add_colum_rate_db_pro : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ProductAuctions", "Rated", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.ProductAuctions", "Rated");
        }
    }
}
